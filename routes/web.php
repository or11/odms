<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/sendMail',function(){
  $data = array(
        'name' => "หลุย",
    );
    Mail::send('dashgum.sendMail', $data, function ($message) {
        $message->from('odmssystem@gmail.com');
        $message->to('aodlinkinpark2009@gmail.com')->subject('ใกล้ถึงกำหนดการคืนเอกสาร');
     });
    return "Send Email Sucessfully";
  });

 
Route::get('/', function () {
    // return view('welcome');
    if(Auth::check()){
    	return redirect('dashboard');
    }else{
    	return redirect('auth/login');
    }
});


Route::post('/auth/login', 'User\AuthController@postLogin');
Route::get('/auth/login', function(){
    if(Auth::check()){
    	return redirect('dashboard');
    }else{
		return view('dashgum.login');
    }
});

Route::get('unauthorize', function(){
	return "You're unauthorize!";
});


/*AUTHEN ZONE*/
Route::group(['middleware' => 'auth'], function(){

	/*ADMIN & MANAGER ZONE*/
	Route::group(['middleware' => 'adminmanager'], function(){

		Route::get('detail/{type}-{id}', 'DB\DBController@detailDocumentByAdmin');

		Route::group(['prefix' => 'doc'], function(){

			Route::get('borrow', 'DB\DBController@viewBorrowList');

			Route::get('approve/{type}-{receive}', 'DB\DBController@makeApprove');
			
			Route::get('file-approve/{type}-{receive}', 'DB\DBController@makeFileApprove');
		});

	});
	
	/*MANAGER ZONE*/
	Route::group(['middleware' => 'manager'], function(){/**/});

	/*ADMIN ZONE*/
	Route::group(['middleware' => 'admin'], function(){

		/**
		 * REPORT ZONE.
		 */
		Route::group(['prefix' => 'report'], function(){
			Route::get('receive', function(){
				$pdf = PDF::loadView('pdf');
				return $pdf->stream();
			});

			Route::get('year-receive', 'DB\DBController@getYearReceiveDocReport');
			Route::get('year-borrow', 'DB\DBController@getYearBorrowReport');
			Route::post('year-borrow', 'DB\DBController@postYearBorrowReport');
		});

		/**
		 * VIEW FORM MASTER DATA
		 */
		Route::group(['prefix' => 'master-data'], function(){
			Route::get('document', 'DB\DBController@getDocumentType');
			Route::post('document','DB\DBController@InsertDocumentType');

			Route::get('position', 'DB\DBController@getPosition');
			Route::post('position', 'DB\DBController@InsertPosition');

			Route::get('chemicals', 'DB\DBController@getChemicals');
			Route::post('chemicals', 'DB\DBController@InsertChemicals');

			Route::post('document','DB\DBController@InsertDocumentType');
			
			Route::get('department', 'DB\DBController@getDepartment');
			Route::post('department','DB\DBController@InsertDepartment');
		});
		/**
		 * ============================================== *
		 */

		Route::group(['prefix' => 'doc'], function(){
			/**
			 * VIEW RECEIVE FORM.
			 */
			Route::group(['prefix'=>'receive'], function(){

				Route::get('msds', 'DB\DBController@getMSDS');
				Route::post('msds', 'DB\DBController@Insertmsds');
				
		 
				Route::get('manual', 'DB\DBController@getManual');
				Route::post('manual', 'DB\DBController@InsertManual');
		 		
				Route::get('standard', 'DB\DBController@getStandard');
				Route::post('standard', 'DB\DBController@Insertstandard');
				
				Route::get('mi', function(){
					return view('dashgum.mi');
				});
				Route::post('mi', 'DB\DBController@InsertMi');

			});
			/**
			 * ============================================== *
			 */

			/**
			 * VIEW FORM MANAGEMENT DOCUMENT.
			 */
			Route::get('return', function(){
				return view('dashgum.returndoc');
			});

			Route::get('return/{type}-{id}', 'DB\DBController@returningDoc');

			/**
			 * ============================================== *
			 */
		});

	});

	/*USER ZONE*/
	Route::group(['middleware' => 'user'], function(){

		Route::get('view/{type}-{id}', 'DB\DBController@detailDocumentByUser');

		Route::get('make/borrow/{type}-{id}', 'DB\DBController@makeBorrow');

		Route::get('make/file-borrow/{type}-{id}', 'DB\DBController@makeFileBorrow');

		Route::get('request-status', 'DB\DBController@getRequestStatus');
	});

	/*ETC ROUTE*/
	Route::get('/dashboard', function(){
		return view('dashgum.index');
	});

	Route::group(['prefix' => 'setting'], function(){
		
		Route::get('user', 'User\ViewUserController@getCredentials');
		Route::get('user-{id}', 'User\ViewUserController@getCredentialsById');
		Route::post('user-add', 'User\ViewUserController@postAddCredentials');
		Route::post('user-edit-{id}', 'User\ViewUserController@postEditCredentials');

	});


	Route::group(['prefix' => 'doc'], function(){
		Route::get('search', function(){
			return view('dashgum.search');
		});	
		Route::post('search','DB\DBController@search');
	});

	Route::get('auth/logout', 'User\AuthController@getLogout');

});