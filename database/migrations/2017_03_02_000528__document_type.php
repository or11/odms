<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DocumentType extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('_document_type', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('dt_id')->unique();
            $table->string('dt_doc_code', 25);
            $table->string('dt_short_name', 10);
            $table->string('dt_long_name', 30);
            $table->enum('dt_status', ['active', 'inactive']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('_document_type');
    }
}
