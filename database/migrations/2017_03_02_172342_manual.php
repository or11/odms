<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Manual extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('manual', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('manual_id')->unique();
            $table->string('manual_doc_code', 25)->unique();
            $table->string('manual_machine_name', 30);
            $table->integer('manual_department_id')->unsigned();
            $table->date('manual_doc_date');
            $table->enum('status', ['active', 'inactive']);
            $table->timestamps();

            /**
             * FOREGIN KEY CONSTRAINT.
             */
            $table->foreign('manual_department_id')->references('dep_id')->on('_department');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('manual');
    }
}
