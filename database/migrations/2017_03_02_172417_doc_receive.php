<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DocReceive extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('doc_receive', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('drec_id')->unique();
            $table->integer('drec_doc_id');
            $table->enum('drec_doc_table', ['msds', 'mi', 'manual', 'standard']);
            $table->string('drec_doc_type_id', 20);
            $table->string('drec_receive_code', 25)->unique();
            $table->date('drec_date');
            $table->string('drec_store_place', 255);
            $table->string('drec_file_path', 255);
            $table->enum('status', ['available', 'borrow', 'pending', 'unavailable']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('doc_receive');
    }
}
