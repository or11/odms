<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class NotificationLogs extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    { 
        Schema::create('notification_log', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('noti_id')->unique();
            $table->integer('noti_borrow_id')->unsigned();
            $table->date('noti_date');
            $table->enum('status', ['success', 'fail']);
            $table->timestamps();

            /**
             * FOREIGN KEY CONSTRAINT
             */
            $table->foreign('noti_borrow_id')->references('dbor_id')->on('doc_borrow');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('notification_log');
    }
}
