<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UsersAddr extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users_addr', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('uaddr_id')->unique();
            $table->integer('uaddr_user_id')->unsigned();
            $table->string('uaddr_no', 6);
            $table->string('uaddr_village_name', 50);
            $table->string('uaddr_street', 50);
            $table->string('uaddr_town', 50);
            $table->string('uaddr_city', 50);
            $table->string('uaddr_province', 50);
            $table->string('uaddr_postal', 6);
            $table->string('uaddr_tel', 25);
            $table->timestamps();

            /**
             * FOREIGN KEY CONSTRAINT
             */
            $table->foreign('uaddr_user_id')->references('usr_id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users_addr');
    }
}
