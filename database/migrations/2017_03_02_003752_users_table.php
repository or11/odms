<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('usr_id')->unique();
            $table->integer('usr_prefix_name')->unsigned();
            $table->integer('usr_position')->unsigned();
            $table->integer('usr_user_role')->unsigned();
            $table->integer('usr_department')->unsigned();
            $table->string('usr_code', 10)->unique();
            $table->string('usr_name', 30);
            $table->string('usr_lastname', 30);
            $table->string('email')->unique();
            $table->string('password', 100);
            $table->integer('usr_age');
            $table->enum('usr_gender', ['male', 'female']);
            $table->string('usr_tel', 20);
            $table->enum('usr_status', ['active', 'inactive']);
            $table->rememberToken();
            $table->timestamps();
            /**
             * FOREIGN KEY CONSTRAINT.
             */
            $table->foreign('usr_prefix_name')->references('pfn_id')->on('_prefix_name');
            $table->foreign('usr_position')->references('pos_id')->on('_position');
            $table->foreign('usr_user_role')->references('ur_id')->on('_user_role');
            $table->foreign('usr_department')->references('dep_id')->on('_department');
            /*================================*/
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
