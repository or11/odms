<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PrefixName extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('_prefix_name', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('pfn_id')->unique();
            $table->string('pfn_short_name', 10);
            $table->string('pfn_long_name', 30);
            $table->enum('pfn_status', ['active', 'inactive']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('_prefix_name');
    }
}
