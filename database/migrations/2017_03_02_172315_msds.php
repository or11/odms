<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Msds extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('msds', function (Blueprint $table) {
            $table->engine = "InnoDB";
            $table->increments('msds_id')->unique();
            $table->string('msds_doc_code', 25)->unique();
            $table->string('msds_chem_name', 50);
            $table->integer('msds_chem_type')->unsigned();
            $table->string('msds_company_name', 50);
            $table->date('msds_doc_date');
            $table->enum('status', ['active', 'inactive']);
            $table->timestamps();

            /**
             * FOREIGN KEY CONSTRAINT
             */
            $table->foreign('msds_chem_type')->references('che_id')->on('_chemicals');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('msds');
    }
}
