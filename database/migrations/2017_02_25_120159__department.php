<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Department extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('_department', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('dep_id')->unique();
            $table->string('dep_short_name', 15);
            $table->string('dep_long_name', 30);
            $table->string('dep_email', 30);
            $table->string('dep_tel', 20);
            $table->enum('dep_status', ['active', 'inactive']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('_department');    }
}
