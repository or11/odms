<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Mi extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mi', function (Blueprint $table) {
            $table->engine ='InnoDB';
            $table->increments('mi_id')->unique();
            $table->string('mi_doc_code', 25)->unique();
            $table->string('mi_doc_no', 25)->unique();
            $table->string('mi_doc_name', 30)->unique();
            $table->integer('mi_change_no');
            $table->string('mi_description', 255);
            $table->string('mi_designer', 30);
            $table->date('mi_doc_date');
            $table->enum('status', ['active', 'inactive']);
            $table->timestamps();            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mi');
    }
}
