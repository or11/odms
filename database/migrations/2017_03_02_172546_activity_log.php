<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ActivityLog extends Migration
{
    /**
     * Run the migrations.
     * @return void
     */
    public function up()
    {
        Schema::create('activity_log', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('log_id')->unique();
            $table->integer('log_user_id')->unsigned();
            $table->string('log_table', 25);
            $table->integer('log_affected_record')->unsigned();
            $table->enum('log_action_type', ['c', 'r', 'u', 'd']);
            $table->enum('status', ['success', 'fail']);
            $table->timestamps();

            /**
             * FOREIGN KEY CONSTRAINT
             */
            $table->foreign('log_user_id')->references('usr_id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('activity_log');
    }
}
