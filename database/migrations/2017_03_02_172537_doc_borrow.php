<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DocBorrow extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('doc_borrow', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('dbor_id')->unique();
            $table->integer('dbor_user_id')->unsigned();
            $table->string('dbor_doc_id');
            $table->enum('dbor_doc_table', ['msds', 'mi', 'manual', 'standard']);
            $table->date('dbor_borrow_date');
            $table->date('dbor_expiry_date');
            $table->date('dbor_return_date')->nullable();
            $table->enum('status', ['borrow', 'return', 'pending', 'cancelled']);
            $table->timestamps();

            /**
             * FOREIGN KEY CONSTRAINT
             */
            $table->foreign('dbor_user_id')->references('usr_id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('doc_borrow');
    }
}
