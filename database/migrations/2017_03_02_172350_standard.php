<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Standard extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('standard', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('standard_id')->unique();
            $table->string('standard_doc_code', 25)->unique();
            $table->string('standard_doc_name', 25);
            $table->string('standard_description', 255);
            $table->integer('standard_doc_type')->unsigned();
            $table->date('standard_doc_date');
            $table->enum('status', ['active', 'inactive']);
            $table->timestamps();

            /**
             * FOREIGN KEY CONSTRAINT
             */
            $table->foreign('standard_doc_type')->references('dt_id')->on('_document_type');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('standard');
    }
}
