<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UserRole extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('_user_role', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('ur_id')->unique();
            $table->string('ur_short_name', 10);
            $table->string('ur_long_name', 30);
            $table->enum('ur_status', ['active', 'inactive']);
            $table->timestamps();
        });        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('_user_role');
    }
}
