<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SubDocument extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sub_document', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('subd_id')->unique();
            $table->integer('subd_doc_master')->unsigned();
            $table->integer('subd_doc_slave')->unsigned();
            $table->string('subd_description', 255);
            $table->enum('subd_doc_type', ['msds', 'mi', 'manual', 'standard']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sub_document');
    }
}
