<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Position extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('_position', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('pos_id')->unique();
            $table->string('pos_short_name', 10);
            $table->string('pos_long_name', 30);
            $table->enum('pos_status', ['active', 'inactive']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('_position');
    }
}
