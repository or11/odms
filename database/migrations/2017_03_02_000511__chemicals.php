<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Chemicals extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('_chemicals', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('che_id')->unique();
            $table->string('che_code', 25);
            $table->string('che_short_name', 10);
            $table->string('che_long_name', 35);
            $table->enum('che_status', ['active', 'inactive']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('_chemicals');
    }
}
