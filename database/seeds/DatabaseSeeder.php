<?php

use Illuminate\Database\Seeder;

/**
 * Use Model
 */
use App\Chemicals;
use App\Department;
use App\DocumentType;
use App\Position;
use App\PrefixName;
use App\UserRole;
use App\User;

class DatabaseSeeder extends Seeder{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(){
        /**
         * Master data
         */
        // $this->call(ChemicalsTableSeeder::class);
        // $this->call(DepartmentTableSeeder::class);
        // $this->call(DocTypeTableSeeder::class);
        // $this->call(PositionTableSeeder::class);
        // $this->call(PrefixNameTableSeeder::class);
        // $this->call(UserRoleTableSeeder::class);

        /**
         * Be mind the foreign key constraint error!
         */        
        $this->call(UsersTableSeeder::class);
    }
}


class UsersTableSeeder extends Seeder{
    /**
     * Seeding tb_users data.
     * @return void
     */
    public function run(){
        if(DB::table('users')->count()>0){
            DB::table('users')->delete();
        }

        User::create([
            "usr_prefix_name" => 1,
            "usr_position" => 2,
            "usr_user_role" => 1,
            "usr_department" => 1,
            "usr_code" => "60743832",
            "usr_name" => "Wesarut",
            "usr_lastname" => "Khm",
            "email" => "root@kce.co.th",
            "password" => Hash::make('temp123456'),
            "usr_age" => 26,
            "usr_gender" => 'male',
            "usr_tel" => '0831601307',
            "usr_status" => 'active'
        ]);
        User::create([
            "usr_prefix_name" => 1,
            "usr_position" => 2,
            "usr_user_role" => 2,
            "usr_department" => 1,
            "usr_code" => "93487848",
            "usr_name" => "Todsavorn",
            "usr_lastname" => "Phl",
            "email" => "manager@kce.co.th",
            "password" => Hash::make('temp123456'),
            "usr_age" => 26,
            "usr_gender" => 'male',
            "usr_tel" => '0831601307',
            "usr_status" => 'active'
        ]);
        User::create([
            "usr_prefix_name" => 1,
            "usr_position" => 2,
            "usr_user_role" => 3,
            "usr_department" => 1,
            "usr_code" => "84932874",
            "usr_name" => "John",
            "usr_lastname" => "Doe",
            "email" => "user@kce.co.th",
            "password" => Hash::make('temp123456'),
            "usr_age" => 26,
            "usr_gender" => 'male',
            "usr_tel" => '0831601307',
            "usr_status" => 'active'
        ]);
    }

}

/**
 * MASTER DATA ZONE.
 */

class UserRoleTableSeeder extends Seeder{
    public function run(){
        if(DB::table('_user_role')->count()>0){
            DB::table('_user_role')->delete();
        }
        UserRole::create([
            "ur_short_name" => 'Admin',
            "ur_long_name" => 'Administrator',
            "ur_status" => 'active'
        ]);
        UserRole::create([
            "ur_short_name" => 'Maganer',
            "ur_long_name" => 'Department Maganer',
            "ur_status" => 'active'
        ]);
        UserRole::create([
            "ur_short_name" => 'User',
            "ur_long_name" => 'General User',
            "ur_status" => 'active'
        ]);
    }
}

class PrefixNameTableSeeder extends Seeder{
    public function run(){
        if(DB::table('_prefix_name')->count()>0){
            DB::table('_prefix_name')->delete();
        }
        PrefixName::create([
            "pfn_short_name" => 'นาย',
            "pfn_long_name" => 'นาย',
            "pfn_status" => 'active'
        ]);
        PrefixName::create([
            "pfn_short_name" => 'นาง',
            "pfn_long_name" => 'นาง',
            "pfn_status" => 'active'
        ]);
        PrefixName::create([
            "pfn_short_name" => 'นส.',
            "pfn_long_name" => 'นางสาว',
            "pfn_status" => 'active'
        ]);
    }
}

class PositionTableSeeder extends Seeder{
    public function run(){
        if(DB::table('_position')->count()>0){
            DB::table('_position')->delete();
        }
        Position::create([
            "pos_short_name" => "SA",
            "pos_long_name" => "System Analyst",
            "pos_status" => 'active'
        ]);
        Position::create([
            "pos_short_name" => "PG",
            "pos_long_name" => "Programmer",
            "pos_status" => 'active'
        ]);
        Position::create([
            "pos_short_name" => "EMP",
            "pos_long_name" => "General Employee",
            "pos_status" => 'active'
        ]);
        Position::create([
            "pos_short_name" => "MNG",
            "pos_long_name" => "Manager",
            "pos_status" => 'active'
        ]);
        Position::create([
            "pos_short_name" => "CEO",
            "pos_long_name" => "Cheif Executive Officer",
            "pos_status" => 'active'
        ]);
    }
}

class DocTypeTableSeeder extends Seeder{
    public function run(){
        if(DB::table('_document_type')->count()>0){
            DB::table('_document_type')->delete();
        }
        DocumentType::create([
            "dt_doc_code" => "DT02983",
            "dt_short_name" => "MIL",
            "dt_long_name" => "MIL Standard",
            "dt_status" => 'active'
        ]);
        DocumentType::create([
            "dt_doc_code" => "DT93928",
            "dt_short_name" => "IPC",
            "dt_long_name" => "IPC Standard",
            "dt_status" => 'active'
        ]);
        DocumentType::create([
            "dt_doc_code" => "DT64334",
            "dt_short_name" => "IEC",
            "dt_long_name" => "IEC Standard",
            "dt_status" => 'active'
        ]);
        DocumentType::create([
            "dt_doc_code" => "DT54378",
            "dt_short_name" => "JIS",
            "dt_long_name" => "JIS Standard",
            "dt_status" => 'active'
        ]);
    }
}

class DepartmentTableSeeder extends Seeder{
    public function run(){
        if(DB::table('_department')->count()>0){
            DB::table('_department')->delete();
        }
        Department::create([
            "dep_short_name" => 'SAFETY',
            "dep_long_name" => 'SAFETY',
            "dep_email" => 'SF@kce.co.th',
            "dep_tel" => '1401',
            "dep_status" => 'active'
        ]);
        Department::create([
            "dep_short_name" => 'WAREHOUSE',
            "dep_long_name" => 'WAREHOUSE',
            "dep_email" => 'WH@kce.co.th',
            "dep_tel" => '1402',
            "dep_status" => 'active'
        ]);
        Department::create([
            "dep_short_name" => 'P.LAB',
            "dep_long_name" => 'P.LAB',
            "dep_email" => 'PL@kce.co.th',
            "dep_tel" => '1402',
            "dep_status" => 'active'
        ]);
        Department::create([
            "dep_short_name" => 'PAT',
            "dep_long_name" => 'PAT',
            "dep_email" => 'PAT@kce.co.th',
            "dep_tel" => '1403',
            "dep_status" => 'active'
        ]);
        Department::create([
            "dep_short_name" => 'LAM',
            "dep_long_name" => 'LAM',
            "dep_email" => 'LAM@kce.co.th',
            "dep_tel" => '1405',
            "dep_status" => 'active'
        ]);
        Department::create([
            "dep_short_name" => 'MC',
            "dep_long_name" => 'MC',
            "dep_email" => 'MC@kce.co.th',
            "dep_tel" => '1406',
            "dep_status" => 'active'
        ]);
        Department::create([
            "dep_short_name" => 'DC',
            "dep_long_name" => 'DC',
            "dep_email" => 'DC@kce.co.th',
            "dep_tel" => '1407',
            "dep_status" => 'active'
        ]);
        Department::create([
            "dep_short_name" => 'MK',
            "dep_long_name" => 'MK',
            "dep_email" => 'MK@kce.co.th',
            "dep_tel" => '1408',
            "dep_status" => 'active'
        ]);
        Department::create([
            "dep_short_name" => 'FAB',
            "dep_long_name" => 'FAB',
            "dep_email" => 'FAB@kce.co.th',
            "dep_tel" => '1409',
            "dep_status" => 'active'
        ]);
        Department::create([
            "dep_short_name" => 'ET',
            "dep_long_name" => 'ET',
            "dep_email" => 'ET@kce.co.th',
            "dep_tel" => '1410',
            "dep_status" => 'active'
        ]);
        Department::create([
            "dep_short_name" => 'MN',
            "dep_long_name" => 'MN',
            "dep_email" => 'MN@kce.co.th',
            "dep_tel" => '1411',
            "dep_status" => 'active'
        ]);
        Department::create([
            "dep_short_name" => 'SM',
            "dep_long_name" => 'SM',
            "dep_email" => 'SM@kce.co.th',
            "dep_tel" => '1412',
            "dep_status" => 'active'
        ]);
        Department::create([
            "dep_short_name" => 'TS',
            "dep_long_name" => 'TS',
            "dep_email" => 'TS@kce.co.th',
            "dep_tel" => '1413',
            "dep_status" => 'active'
        ]);
        Department::create([
            "dep_short_name" => 'TC',
            "dep_long_name" => 'TC',
            "dep_email" => 'TC@kce.co.th',
            "dep_tel" => '1413',
            "dep_status" => 'active'
        ]);
        Department::create([
            "dep_short_name" => 'QC',
            "dep_long_name" => 'QC',
            "dep_email" => 'QC@kce.co.th',
            "dep_tel" => '1414',
            "dep_status" => 'active'
        ]);
        Department::create([
            "dep_short_name" => 'MS',
            "dep_long_name" => 'MS',
            "dep_email" => 'MS@kce.co.th',
            "dep_tel" => '1415',
            "dep_status" => 'active'
        ]);
    }
}

class ChemicalsTableSeeder extends Seeder{
    
    public function run(){
        
    	// DB::table('_chemicals')->delete();
        if(DB::table('_chemicals')->count()>0){
            DB::table('_chemicals')->delete();
        }

        Chemicals::create([
            "che_code" => 'chm938934',
            "che_short_name" => 'CHM',
            "che_long_name" => 'Chemical',
            "che_status" => 'active'
        ]);
        Chemicals::create([
            "che_code" => 'chm939497',
            "che_short_name" => 'COPF',
            "che_long_name" => 'COPPER FOIL',
            "che_status" => 'active'
        ]);
        Chemicals::create([
            "che_code" => 'chm857497',
            "che_short_name" => 'LP',
            "che_long_name" => 'LINK PRINTHING',
            "che_status" => 'active'
        ]);
    	Chemicals::create([
            "che_code" => 'chm938272',
            "che_short_name" => 'P&L',
            "che_long_name" => 'Prepreg & Laminate',
            "che_status" => 'active'
        ]);
    }

}