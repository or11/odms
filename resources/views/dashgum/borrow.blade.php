@php
	$pagename = "รายการยืมเอกสาร"
@endphp
@extends('dashgum.layout.index_layout')
@section('title_site', $pagename)
@section('header_title', "ODMS")


@section('content_section')
<h2>{{$pagename}}</h2>
	<div class="col-md-12 mt">
      	<div class="content-panel">
			<table class="table table-hover" id="tb_document">
				<h4><i class="fa fa-angle-right"></i> รายการยืมเอกสาร</h4>
				<hr>
				<thead>
					<tr>
						<th>#</th>
						<th>เลขที่รับ</th>
						<th>ประเภทเอกสาร</th>
						<th>ผู้ยืม</th>
						<th>แผนก</th>
						<th>การอนุมัติ</th>
					</tr>
				</thead>
	@if(isset($result) && sizeof($result) > 0)
				<tbody>
				@php($count = 0)
				@foreach($result as $r)
					@php
						$rec = base64_encode($r->drec_receive_code);
					@endphp
					<tr>
						<td>{{++$count}}</td>
						<td>{{$r->drec_receive_code}}</td>
						<td>{{$r->drec_doc_table}}</td>
						<td>
							{{$r->usr_name}} {{$r->usr_lastname}} <br>
							<small><a href="mailto:{{$r->email}}">{{$r->email}}</a></small>
						</td>
						<td>
							{{$r->dep_long_name}} <br>
							<small><a href="mailto:{{$r->dep_email}}">{{$r->dep_email}}</a></small>
						</td>
						<td>
							<a href="{{url('doc/approve/pass-'.$rec)}}" 
								class="btn btn-xs btn-success">อนุมัติ</a>
							<a href="{{url('doc/approve/cancel-'.$rec)}}" 
								class="btn btn-xs btn-danger">ไม่อนุมัติ</a>
						</td>
					</tr>
				@endforeach
				</tbody>
    @else
    	<h2>DATA NOT FOUND</h2>
	@endif
			</table>
      	  </div><!-- content-panel -->
      </div><!-- /col-md-12 -->

	<div class="col-md-12 mt">
      	<div class="content-panel">
			<table class="table table-hover" id="tb_document">
				<h4><i class="fa fa-angle-right"></i> รายการขอไฟล์เอกสาร</h4>
				<hr>
				<thead>
					<tr>
						<th>#</th>
						<th>เลขที่รับ</th>
						<th>ประเภทเอกสาร</th>
						<th>ผู้ยืม</th>
						<th>แผนก</th>
						<th>การอนุมัติ</th>
					</tr>
				</thead>
	@if(isset($file) && sizeof($file) > 0)
				<tbody>
				@php($count = 0)
				@foreach($file as $r)
					@php
						$rec = base64_encode($r->drec_receive_code);
					@endphp
					<tr>
						<td>{{++$count}}</td>
						<td>{{$r->drec_receive_code}}</td>
						<td>{{$r->drec_doc_table}}</td>
						<td>
							{{$r->usr_name}} {{$r->usr_lastname}} <br>
							<small><a href="mailto:{{$r->email}}">{{$r->email}}</a></small>
						</td>
						<td>
							{{$r->dep_long_name}} <br>
							<small><a href="mailto:{{$r->dep_email}}">{{$r->dep_email}}</a></small>
						</td>
						<td>
							<a href="{{url('doc/file-approve/pass-'.$rec)}}" 
								class="btn btn-xs btn-success">อนุมัติ</a>
							<a href="{{url('doc/file-approve/cancel-'.$rec)}}" 
								class="btn btn-xs btn-danger">ไม่อนุมัติ</a>
						</td>
					</tr>
				@endforeach
				</tbody>
    @else
    	<h2>DATA NOT FOUND</h2>
	@endif
			</table>
      	  </div><!-- content-panel -->
      </div><!-- /col-md-12 -->
@endsection

@section('onready_section')
<script>
	$(document).ready(function() {
		// $("#tb_document").DataTable();
	});
</script>
@endsection