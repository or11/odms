@section('javascript')
 	<!-- js placed at the end of the document so the pages load faster -->
    <script src="{{asset('dashgum/js/jquery.js')}}"></script>
    <script src="{{asset('dashgum/js/jquery-1.8.3.min.js')}}"></script>
    <script src="{{asset('dashgum/js/bootstrap.min.js')}}"></script>
    <script class=include type="text/javascript" src="{{asset('dashgum/js/jquery.dcjqaccordion.2.7.js')}}"></script>
    <script src="{{asset('dashgum/js/jquery.scrollTo.min.js')}}"></script>
    <script src="{{asset('dashgum/js/jquery.nicescroll.js')}}" type="text/javascript"></script>
    <script src="{{asset('dashgum/js/jquery.sparkline.js')}}"></script>


    <!--common script for all pages-->
    <script src="{{asset('dashgum/js/common-scripts.js')}}"></script>
    
    <script type="text/javascript" src="{{asset('dashgum/js/gritter/js/jquery.gritter.js')}}"></script>
    <script type="text/javascript" src="{{asset('dashgum/js/gritter-conf.js')}}"></script>

    <!--script for this page-->
    <script src="{{asset('dashgum/js/sparkline-chart.js')}}"></script>    
	<script src="{{asset('dashgum/js/zabuto_calendar.js')}}"></script>	

    <!-- DataTable -->
    <script src="//cdn.datatables.net/1.10.13/js/jquery.dataTables.min.js"></script>
@endsection