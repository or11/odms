@section('stylesheet')
<!-- Bootstrap core CSS -->
    <link href="{{asset('dashgum/css/bootstrap.css')}}" rel="stylesheet">
    <!--external css-->
    <link href="{{asset('dashgum/font-awesome/css/font-awesome.css')}}" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="{{asset('dashgum/css/zabuto_calendar.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('dashgum/js/gritter/css/jquery.gritter.css')}}" />
    <link rel="stylesheet" type="text/css" href="{{asset('dashgum/lineicons/style.css')}}">    
    
    <!-- Custom styles for this template -->
    <link href="{{asset('dashgum/css/style.css')}}" rel="stylesheet">
    <link href="{{asset('dashgum/css/style-responsive.css')}}" rel="stylesheet">

    <!-- DataTable -->
    <link rel="stylesheet" href="//cdn.datatables.net/1.10.13/css/jquery.dataTables.min.css">

    <script src="{{asset('dashgum/js/chart-master/Chart.js')}}"></script>
    
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
@endsection