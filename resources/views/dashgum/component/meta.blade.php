@section('meta')
	<meta charset="utf-8">
	<link rel="icon" href="{{asset('dashgum/img/favicon.ico')}}" type="image/x-icon" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="Dashboard">
    <meta name="keyword" content="Dashboard, Bootstrap, Admin, Template, Theme, Responsive, Fluid, Retina">
    <title>ODMS - @yield('title_site')</title>
@endsection