@section('content')
<div class="row">
	<div class="col-lg-9 main-chart">
		@yield('content_section')
	</div><!-- /col-lg-9 END SECTION MIDDLE -->
			
			<!-- **********************************************************************************************************************************************************
			RIGHT SIDEBAR CONTENT
			*********************************************************************************************************************************************************** -->
			
	<div class="col-lg-3 ds">
		@include('dashgum.component.right_side')
		@yield('right_side')
	</div><!-- /col-lg-3 -->
</div><! --/row -->
@endsection