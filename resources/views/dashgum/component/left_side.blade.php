@section('left_side')
<div id="sidebar" class="nav-collapse ">
	<!-- sidebar menu start-->
	<ul class="sidebar-menu" id="nav-accordion">
		
		<p class="centered"><a href="profile.html"><img src="{{asset('dashgum/img/ui-sam.jpg')}}" class="img-circle" width="60"></a></p>
		<h5 class="centered">
			@if(Auth::check())
				{{Auth::user()->usr_name}} {{Auth::user()->usr_lastname}}
			@else
				Online Document Management System
			@endif
		</h5>
		<li class="mt">
			<a href="{{url('dashboard')}}">
				<i class="fa fa-dashboard"></i>
				<span>Dashboard</span>
			</a>
		</li>
<!-- ADMIN -->
@if(Auth::user()->isAdmin())
		<li class="sub-menu" class="active">
			<a href="javascript:;">
				<i class=" fa fa-bar-chart-o"></i>
				<span>ค้นหา</span>
			</a>
			<ul class="sub">
				<li><a href="{{url('doc/search')}}">เอกสาร</a></li>
			</ul>
		</li>
		<li class="sub-menu">
			<a href="javascript:;">
				<i class=" fa fa-bar-chart-o"></i>
				<span>ยืม / คืน</span>
			</a>
			<ul class="sub">
				<li><a href="{{url('doc/borrow')}}">อนุมัติการยืม</a></li>
				<li><a href="{{url('doc/search')}}">คืนเอกสาร</a></li>
			</ul>
		</li>
		<li class="sub-menu" class="active">
			<a href="javascript:;">
				<i class=" fa fa-bar-chart-o"></i>
				<span>นำเข้าเอกสาร</span>
			</a>
			<ul class="sub">
				<li><a href="{{url('doc/receive/msds')}}">MSDS</a></li>
				<li><a href="{{url('doc/receive/manual')}}">MANUAL</a></li>
				<li><a href="{{url('doc/receive/mi')}}">MI</a></li>
				<li><a href="{{url('doc/receive/standard')}}">STANDARD</a></li>
			</ul>
		</li>
		<li class="sub-menu">
			<a href="javascript:;">
				<i class=" fa fa-bar-chart-o"></i>
				<span>ข้อมูลเริ่มต้น</span>
			</a>
			<ul class="sub">
				<li><a href="{{url('master-data/document')}}">ประเภทเอกสาร</a></li>
				<li><a href="{{url('master-data/department')}}">แผนก</a></li>
				<li><a href="{{url('master-data/position')}}">ตำแหน่งงาน</a></li>
				<li><a href="{{url('master-data/chemicals')}}">สารเคมี</a></li>
			</ul>
		</li>
		<li class="sub-menu">
			<a href="javascript:;">
				<i class=" fa fa-bar-chart-o"></i>
				<span>ผู้ใช้งาน</span>
			</a>
			<ul class="sub">
				<li><a href="{{url('setting/user')}}">จัดการผู้ใช้</a></li>
			</ul>
		</li>
		<li class="sub-menu">
			<a href="javascript:;">
				<i class=" fa fa-bar-chart-o"></i>
				<span>รายงาน</span>
			</a>
			<ul class="sub">
				<li><a href="{{url('report/year-receive')}}" target="_blank">รายการรับเข้าเอกสาร</a></li>
				<li><a href="{{url('report/year-borrow')}}">รายงานการยืมเอกสาร</a></li>
			</ul>
		</li>
<!-- ADMIN -->
<!-- MANAGER -->
@elseif(Auth::user()->isManager())
		<li class="sub-menu" class="active">
			<a href="javascript:;">
				<i class=" fa fa-bar-chart-o"></i>
				<span>ค้นหา</span>
			</a>
			<ul class="sub">
				<li><a href="{{url('doc/search')}}">เอกสาร</a></li>
			</ul>
		</li>
		<li class="sub-menu">
			<a href="javascript:;">
				<i class=" fa fa-bar-chart-o"></i>
				<span>ยืม / คืน</span>
			</a>
			<ul class="sub">
				<li><a href="{{url('doc/borrow')}}">อนุมัติการยืม</a></li>
				<li><a href="{{url('doc/search')}}">คืนเอกสาร</a></li>
			</ul>
		</li>
<!-- MANAGER -->
<!-- USER -->
@elseif(Auth::user()->isUser())

		<li class="sub-menu" class="active">
			<a href="{{url('request-status')}}">
				<i class=" fa fa-bar-chart-o"></i>
				<span>สถานะคำร้อง</span>
			</a>
		</li>
		<li class="sub-menu" class="active">
			<a href="javascript:;">
				<i class=" fa fa-bar-chart-o"></i>
				<span>ค้นหา</span>
			</a>
			<ul class="sub">
				<li><a href="{{url('doc/search')}}">เอกสาร</a></li>
			</ul>
		</li>
@endif
<!-- USER -->
<!-- ETC -->
<!-- ETC -->
	</ul>
	<!-- sidebar menu end-->
</div>
@endsection