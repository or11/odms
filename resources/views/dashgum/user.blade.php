@php
	$pagename = "ผู้ใช้งานระบบ"
@endphp
@extends('dashgum.layout.index_layout')
@section('title_site', $pagename)
@section('header_title', "ODMS")


@section('content_section')
<h2>{{$pagename}}</h2>
@if($errors->any())
	@foreach($errors as $err)
<div class="alert alert-danger alert-dismissable">
	<button type="button" data-dismiss="alert" aria-hidden="true" class="close">x</button>
	<Strong>มีบางอย่างผิดพลาด!</Strong>
	{{$err}}
</div>
	@endforeach
@endif
@if(session('errMSG'))
<div class="alert alert-danger alert-dismissable">
	<button type="button" data-dismiss="alert" aria-hidden="true" class="close">x</button>
	<Strong>มีบางอย่างผิดพลาด!</Strong>
	{{session('errMSG')}}
</div>
@endif
@if(session('success'))
<div class="alert alert-success alert-dismissable">
	<button type="button" data-dismiss="alert" aria-hidden="true" class="close">x</button>
	<Strong>สำเร็จ!</Strong>
	{{session('success')}}
</div>
@endif

<div class="row mt">
	<div class="col-lg-12">
		<div class="form-panel">
			<h4 class="mb"><i class="fa fa-angle-right"></i> ข้อมูลผู้ใช้งานระบบ</h4>
			@if(isset($result['user']))
				@php($action = 'setting/user-edit-'.$result['user'][0]->usr_id)
			@else
				@php($action = 'setting/user-add')
			@endif
			<form class="form-horizontal style-form" method="post" action="{{url($action)}}">
				<div class="form-group">
					<label class="col-sm-2 control-label">คำนำหน้าชื่อ<br>name title</br></label>
					<div class="col-sm-10">
						<select name="usr_prefix_name" class="form-control" id="">
							@if(isset($result['prefixName']) && sizeof($result['prefixName']) > 0)
								@foreach($result['prefixName'] as $pfn)
									@php($select = "")
									@if(isset($result['user'][0]->usr_prefix_name) && ($result['user'][0]->usr_prefix_name == $pfn->pfn_id))
										@php($select = "selected")
									@endif
							<option value="{{$pfn->pfn_id}}" {{$select}}>{{$pfn->pfn_long_name}}</option>
								@endforeach
							@endif
						</select>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-2 control-label">แผนก<br>department</br></label>
					<div class="col-sm-10">
						<select name="usr_department" class="form-control" id="">
							@if(isset($result['department']) && sizeof($result['department']) > 0)
								@foreach($result['department'] as $dep)
									@php($select = "")
									@if(isset($result['user'][0]->usr_department) && ($result['user'][0]->usr_department == $dep->dep_id))
										@php($select = "selected")
									@endif
							<option value="{{$dep->dep_id}}" {{$select}}>{{$dep->dep_long_name}}</option>
								@endforeach
							@endif
						</select>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-2 control-label">ตำแหน่ง<br>position</br></label>
					<div class="col-sm-10">
						<select name="usr_position" class="form-control" id="">
							@if(isset($result['position']) && sizeof($result['position']) > 0)
								@foreach($result['position'] as $pos)
									@php($select = "")
									@if(isset($result['user'][0]->usr_position) && ($result['user'][0]->usr_position == $pos->pos_id))
										@php($select = "selected")
									@endif
							<option value="{{$pos->pos_id}}" {{$select}}>{{$pos->pos_long_name}}</option>
								@endforeach
							@endif
						</select>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-2 control-label">ระดับผู้ใช้ระบบ<br>user role level</br></label>
					<div class="col-sm-10">
						<select name="usr_user_role" class="form-control" id="">
							@if(isset($result['userRole']) && sizeof($result['userRole']) > 0)
								@foreach($result['userRole'] as $ur)
									@php($select = "")
									@if(isset($result['user'][0]->usr_user_role) && ($result['user'][0]->usr_user_role == $ur->ur_id))
										@php($select = "selected")
									@endif
							<option value="{{$ur->ur_id}}" {{$select}}>{{$ur->ur_long_name}}</option>
								@endforeach
							@endif
						</select>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-2 col-sm-2 control-label">เลขที่พนักงาน<br>user id</br></label>
					<div class="col-sm-10">
						@if(isset($result['user'][0]))
							@php($usr_code = $result['user'][0]->usr_code)
						@else
							@php($usr_code = "")
						@endif
						<input type="text" class="form-control" name="usr_code" value="{{$usr_code}}">
					</div>
				</div>
				<div class="form-group">
					@if(isset($result['user'][0]))
						@php($usr_name = $result['user'][0]->usr_name)
						@php($usr_lastname = $result['user'][0]->usr_lastname)
					@else
						@php($usr_name = "")
						@php($usr_lastname = "")
					@endif
					<label class="col-sm-2 col-sm-2 control-label">ชื่อ นามสกุล<br>name lastname</br></label>
					<div class="col-sm-5">
						<input type="text" class="form-control" name="usr_name" placeholder="ชื่อ" value="{{$usr_name}}">
					</div>
					<div class="col-sm-5">
						<input type="text" class="form-control" name="usr_lastname" placeholder="นามสกุล" value="{{$usr_lastname}}">
					</div>
				</div>
				<div class="form-group">
					@if(isset($result['user'][0]))
						@php($email = $result['user'][0]->email)
					@else
						@php($email = "")
					@endif
					<label class="col-sm-2 col-sm-2 control-label">อีเมล<br>email</br></label>
					<div class="col-sm-10">
						<input type="mail" class="form-control" name="email" value="{{$email}}">
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-2 col-sm-2 control-label">รหัสผ่าน<br>password</br></label>
					<div class="col-sm-10">
						<input type="password" class="form-control" name="password">
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-2 col-sm-2 control-label">ใส่รหัสผ่านอีกครั้ง<br>confirm password</br></label>
					<div class="col-sm-10">
						<input type="password" class="form-control" name="conf_password">
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-2 control-label">อายุ<br>age</age></label>
					<div class="col-sm-3">
						<select name="usr_age" class="form-control" id="">
						@for($i=20;$i<=50;$i++)
							@php($select = "")
							@if(isset($result['user'][0]->usr_age) && ($result['user'][0]->usr_age == $i))
								@php($select = "selected")
							@endif
							<option value="{{$i}}" {{$select}}>{{$i}}</option>
						@endfor
						</select>
					</div>
				</div>
				<div class="form-group">
					@if(isset($result['user'][0]))
						@php($usr_tel = $result['user'][0]->usr_tel)
					@else
						@php($usr_tel = "")
					@endif
					<label class="col-sm-2 col-sm-2 control-label">โทรศัพท์มือถือ<br>number phone</br></label>
					<div class="col-sm-10">
						<input type="tel" class="form-control" name="usr_tel" value="{{$usr_tel}}">
					</div>
				</div>
				<div class="form-group">
					@if(isset($result['user'][0]))
						@php($uaddr_tel = $result['user'][0]->uaddr_tel)
					@else
						@php($uaddr_tel = "")
					@endif
					<label class="col-sm-2 col-sm-2 control-label">โทรศัพท์บ้าน<br>telephone</br></label>
					<div class="col-sm-10">
						<input type="tel" class="form-control" name="uaddr_tel" value="{{$uaddr_tel}}">
					</div>
				</div>
				<div class="radio">
					@if(isset($result['user'][0]) && ($result['user'][0]->usr_gender == "female"))
						@php($female = "checked")
						@php($male = "")
					@else
						@php($male = "checked")
						@php($female = "")
					@endif
					<h5>เพศ</h5></br>
					<label>
						<input type="radio" name="usr_gender" id="radio_active" value="male" {{$male}}>
						ชาย
					</label>
				</div>
				<div class="radio">
					<label>
						<input type="radio" name="usr_gender" id="radio_inactive" value="female" {{$female}}>
						หญิง
					</label>
				</div>
				<div class="radio">
					@if(isset($result['user'][0]) && ($result['user'][0]->usr_status == "inactive"))
						@php($inactive = "checked")
						@php($active = "")
					@else
						@php($active = "checked")
						@php($inactive = "")
					@endif
					<h4 class="mb"><i class="fa fa-angle-right"></i> สถานะผู้ใช้</h4><br><h5>status user</h5></br>
					<label>
						<input type="radio" name="usr_status" id="radio_active" value="active" {{$active}}>
						Active
					</label>
				</div>
				<div class="radio">
					<label>
						<input type="radio" name="usr_status" id="radio_inactive" value="inactive" {{$inactive}}>
						Inactive
					</label>
				</div>
		</div>
	</div><!-- col-lg-12-->
</div>
<div class="row mt">
	<div class="col-lg-12">
		<div class="form-panel">
			<h4 class="mb"><i class="fa fa-angle-right"></i> ที่อยู่อาศัย</h4><h5><br>address</br></h5>
			<div class="form-horizontal style-form">
				<div class="form-group">
					@if(isset($result['user'][0]))
						@php($uaddr_no = $result['user'][0]->uaddr_no)
					@else
						@php($uaddr_no = "")
					@endif
					<label class="col-sm-2 col-sm-2 control-label">เลขที่<br>house no</br></label>
					<div class="col-sm-10">
						<input type="text" class="form-control" name="uaddr_no" value="{{$uaddr_no}}">
					</div>
				</div>
				<div class="form-group">
					@if(isset($result['user'][0]))
						@php($uaddr_village_name = $result['user'][0]->uaddr_village_name)
					@else
						@php($uaddr_village_name = "")
					@endif
					<label class="col-sm-2 col-sm-2 control-label">ชื่อหมู่บ้าน<br>village name</br></label>
					<div class="col-sm-10">
						<input type="text" class="form-control" name="uaddr_village_name" 
							value="{{$uaddr_village_name}}">
					</div>
				</div>
				<div class="form-group">
					@if(isset($result['user'][0]))
						@php($uaddr_street = $result['user'][0]->uaddr_street)
					@else
						@php($uaddr_street = "")
					@endif
					<label class="col-sm-2 col-sm-2 control-label">ถนน<br>road</br></label>
					<div class="col-sm-10">
						<input type="text" class="form-control" name="uaddr_street" value="{{$uaddr_street}}">
					</div>
				</div>
				<div class="form-group">
					@if(isset($result['user'][0]))
						@php($uaddr_town = $result['user'][0]->uaddr_town)
					@else
						@php($uaddr_town = "")
					@endif
					<label class="col-sm-2 col-sm-2 control-label">ตำบล/แขวง<br>sub-district/sub-area</br></label>
					<div class="col-sm-4">
						<input type="text" class="form-control" name="uaddr_town" value="{{$uaddr_town}}">
					</div>
					@if(isset($result['user'][0]))
						@php($uaddr_city = $result['user'][0]->uaddr_city)
					@else
						@php($uaddr_city = "")
					@endif
					<label class="col-sm-2 col-sm-2 control-label">อำเภอ/เขต<br>district / area</br></label>
					<div class="col-sm-4">
						<input type="text" class="form-control" name="uaddr_city" value="{{$uaddr_city}}">
					</div>
				</div>
				<div class="form-group">
					@if(isset($result['user'][0]))
						@php($uaddr_province = $result['user'][0]->uaddr_province)
					@else
						@php($uaddr_province = "")
					@endif
					<label class="col-sm-2 col-sm-2 control-label">จังหวัด<br>province</br></label>
					<div class="col-sm-4">
						<input type="text" class="form-control" name="uaddr_province" value="{{$uaddr_province}}">
					</div>
					@if(isset($result['user'][0]))
						@php($uaddr_postal = $result['user'][0]->uaddr_postal)
					@else
						@php($uaddr_postal = "")
					@endif
					<label class="col-sm-2 col-sm-2 control-label">รหัสไปรษณีย์<br>postal code</br></label>
					<div class="col-sm-4">
						<input type="text" class="form-control" name="uaddr_postal" value="{{$uaddr_postal}}">
					</div>
				</div>
				<div class="form-group">
					<div class="col-sm-12 text-right">
						<input type="submit" class="btn btn-success" value="เพิ่มรายการ">
						<!-- HIDDEN DATA -->
						<input type="hidden" name="_token" value="{{csrf_token()}}">
					</div>
				</div>
			</div>
			</form>
		</div>
	</div><!-- col-lg-12-->
</div>
	@if(isset($result['users']) && sizeof($result['users']) > 0)
	<div class="col-md-12 mt">
      	<div class="content-panel">
			<table class="table table-hover" id="tb_document">
				<h4><i class="fa fa-angle-right"></i> รายการตำแหน่งงาน</h4>
				<hr>
				<thead>
					<tr>
						<th>#</th>
						<th>รหัส</th>
						<th>ชื่อ-สกุล</th>
						<th>เพศ</th>
						<th>โทรศัพท์</th>
						<th>จัดการ</th>
					</tr>
				</thead>
				<tbody>
				@php($count = 0)
				@foreach($result['users'] as $user)
					<tr>
						<td>{{++$count}}</td>
						<td>{{$user->usr_code}}</td>
						<td>{{$user->usr_name}} {{$user->usr_lastname}}<br> 
							<small>{{$user->email}}</small>
						</td>
						<td>{{$user->usr_gender}}</td>
						<td>{{$user->usr_tel}}</td>
						<td>
							<div class="btn-group">
							  <button type="button" class="btn btn-warning dropdown-toggle" 
							  	data-toggle="dropdown">
							    จัดการ <span class="caret"></span>
							  </button>
							  <ul class="dropdown-menu" role="menu">
							    <!-- <li><a href="#">เรียกดู</a></li> -->
							    <li><a href="{{url('setting/user-'.$user->usr_id)}}">แก้ไข</a></li>
							  </ul>
							</div>
						</td>
					</tr>
				@endforeach
				</tbody>
			</table>
      	  </div><!-- content-panel -->
      </div><!-- /col-md-12 -->
    @else
    	<h2>DATA NOT FOUND</h2>
	@endif
	<br>
	<br>
	<br>
@endsection

@section('onready_section')
<script>
	$(document).ready(function() {
		$("#tb_document").DataTable();
	});
</script>
@endsection