@extends('dashgum.layout.index_layout')
@section('title_site', "ประเภทเอกสาร")
@section('header_title', "ODMS")


@section('content_section')
<h2>ประเภทเอกสาร</h2>
<div class="row mt">
	<div class="col-lg-12">
		<div class="form-panel">
			<h4 class="mb"><i class="fa fa-angle-right"></i> ข้อมูลประเภทเอกสาร</h4>
			<form class="form-horizontal style-form" method="post" action="{{url('master-data/document')}}">
				<div class="form-group">
					<label class="col-sm-2 col-sm-2 control-label">รหัสประเภทเอกสาร<br>category number</br></label>
					<div class="col-sm-10">
						<input type="text" class="form-control" name="dt_doc_code" required>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-2 col-sm-2 control-label">ชื่อเอกสาร<br>document name</br></label>
					<div class="col-sm-10">
						<input type="text" class="form-control" name="dt_long_name" required>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-2 col-sm-2 control-label">ชื่อเอกสารแบบย่อ<br>document name shot</br></label>
					<div class="col-sm-10">
						<input type="text" class="form-control" name="dt_short_name" required>
					</div>
				</div>
				<div class="radio">
					<h4 class="mb"><i class="fa fa-angle-right"></i>สถานะเอกสาร</h4><br><h5>document status</h5></br>
					<label>
						<input type="radio" name="dt_status" id="radio_active" value="active" checked>
						Active
					</label>
				</div>
				<div class="radio">
					<label>
						<input type="radio" name="dt_status" id="radio_inactive" value="inactive">
						Inactive
					</label>
				</div>
				<div class="form-group">
					<div class="col-sm-12 text-right">
						<input type="submit" class="btn btn-success" value="เพิ่มรายการ">
						<!-- HIDDEN DATA -->
						<input type="hidden" name="_token" value="{{csrf_token()}}">
					</div>
				</div>
			</form>
		</div>
	</div><!-- col-lg-12-->
</div>
	@if(isset($result))
	<div class="col-md-12 mt">
      	<div class="content-panel">
			<table class="table table-hover" id="tb_document">
				<h4><i class="fa fa-angle-right"></i> รายการประเภทเอกสาร</h4>
				<hr>
				<thead>
					<tr>
						<th>#</th>
						<th>Code</th>
						<th>Short Name</th>
						<th>Status</th>
					</tr>
				</thead>

				<tbody>
			@php($count = 0)
			@foreach($result as $doc)
					<tr>
						<td>{{++$count}}</td>
						<td>{{$doc->dt_doc_code}}</td>
						<td>{{$doc->dt_short_name}}</td>
						<td>{{$doc->dt_status}}</td>
					</tr>
			@endforeach
				</tbody>
			</table>
      	  </div><!-- content-panel -->
      </div><!-- /col-md-12 -->
    @else
    	<h2>DATA NOT FOUND</h2>
	@endif
@endsection

@section('onready_section')
<script>
	$(document).ready(function() {
		$("#tb_document").DataTable();
	});
</script>
@endsection