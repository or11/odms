@php
	$pagename = "สถานะคำร้อง"
@endphp
@extends('dashgum.layout.index_layout')
@section('title_site', $pagename)
@section('header_title', "ODMS")


@section('content_section')
<h2>{{$pagename}}</h2>
	<div class="col-md-12 mt">
      	<div class="content-panel">
			<table class="table table-hover" id="tb_document">
				<h4><i class="fa fa-angle-right"></i> คำร้องขอยืมเอกสาร</h4>
				<hr>
				<thead>
					<tr>
						<th>#</th>
						<th>ประเภท</th>
						<th>ชื่อ</th>
						<th>สถานะ</th>
					</tr>
				</thead>
				<tbody>
	@if(isset($result['borrow']))
		@php($count = 0)
		@foreach($result['borrow'] as $b)
			@php
				$name = "";
				switch($b->dbor_doc_table){
					case 'mi':
						$name = $b->mi_doc_name;
						break;
					case 'manual':
						$name = $b->manual_machine_name;
						break;
					case 'standard':
						$name = $b->standard_doc_name;
						break;
					case 'msds':
						$name = $b->msds_chem_name;
						break;
					default:
						$name = "N/A";
				}
			@endphp
					<tr>
						<td>{{++$count}}</td>
						<td>{{$b->dbor_doc_table}}</td>
						<td>{{$name}}</td>
						<td>{{$b->status}}</td>
					</tr>				
		@endforeach
	@endif
				</tbody>
			</table>
      	  </div><!-- content-panel -->
      </div><!-- /col-md-12 -->

	<div class="col-md-12 mt">
      	<div class="content-panel">
			<table class="table table-hover" id="tb_document2">
				<h4><i class="fa fa-angle-right"></i> คำร้องขอเปิดไฟล์เอกสาร</h4>
				<hr>
				<thead>
					<tr>
						<th>#</th>
						<th>ประเภท</th>
						<th>ชื่อ</th>
						<th>สถานะ</th>
						<th>ไฟล์</th>
					</tr>
				</thead>
				<tbody>
	@if(isset($result['file']))
		@php($count = 0)
		@foreach($result['file'] as $b)
			@php
				$name = "";
				$path = "";
				$link = "N/A";
				switch($b->dbor_doc_table){
					case 'mi':
						$name = $b->mi_doc_name;
						$path = $b->status == 'borrow' ? $b->drec_file_path : '#';
						$link = $b->status == 'borrow' ? "เปิดไฟล์" : 'N/A';
						break;
					case 'manual':
						$name = $b->manual_machine_name;
						$path = $b->status == 'borrow' ? $b->drec_file_path : '#';
						$link = $b->status == 'borrow' ? "เปิดไฟล์" : 'N/A';
						break;
					case 'standard':
						$name = $b->standard_doc_name;
						$path = $b->status == 'borrow' ? $b->drec_file_path : '#';
						$link = $b->status == 'borrow' ? "เปิดไฟล์" : 'N/A';
						break;
					case 'msds':
						$name = $b->msds_chem_name;
						$path = $b->status == 'borrow' ? $b->drec_file_path : '#';
						$link = $b->status == 'borrow' ? "เปิดไฟล์" : 'N/A';
						break;
					default:
						$name = "N/A";
						$path = "N/A";
				}
			@endphp
					<tr>
						<td>{{++$count}}</td>
						<td>{{$b->dbor_doc_table}}</td>
						<td>{{$name}}</td>
						<td>{{$b->status}}</td>
						<td>
							@if($link == "N/A")
							{{$link}}
							@else
							<a href="{{url($path)}}" target="_blank">{{$link}}</a>
							@endif
						</td>
					</tr>				
		@endforeach
	@endif				
				</tbody>
			</table>
      	  </div><!-- content-panel -->
      </div><!-- /col-md-12 -->
@endsection

@section('onready_section')
<script>
	$(document).ready(function() {
		$("#tb_document").DataTable();
		$("#tb_document2").DataTable();
	});
</script>
@endsection