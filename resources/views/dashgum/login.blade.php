@extends('dashgum.layout.login_layout')

@section('title_site', "Authentication")
@section('head_title', "odms welcome")


@section('content_section')
  CONTENT HERE...
@endsection

@section('mail_error')
	@if($errors->has('usr_mail'))
		@foreach($errors->get('usr_mail') as $msg)
			{{$msg}}
		@endforeach
    @endif
@endsection

@section('pwd_error')
	@if($errors->has('usr_password'))
		@foreach($errors->get('usr_password') as $msg)
			{{$msg}}
		@endforeach
    @endif
@endsection