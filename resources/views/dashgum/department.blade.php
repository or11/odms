@php
	$pagename = "แผนก"
@endphp
@extends('dashgum.layout.index_layout')
@section('title_site', $pagename)
@section('header_title', "ODMS")


@section('content_section')
<h2>{{$pagename}}</h2>
<div class="row mt">
	<div class="col-lg-12">
		<div class="form-panel">
			<h4 class="mb"><i class="fa fa-angle-right"></i> ข้อมูลแผนก</h4>
			<form class="form-horizontal style-form" method="post" action="{{url('master-data/department')}}">
				<div class="form-group">
					<label class="col-sm-2 col-sm-2 control-label">ชื่อแผนก<br>department name</br></label>
					<div class="col-sm-10">
						<input type="text" class="form-control" name="dep_long_name" required>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-2 col-sm-2 control-label">ชื่อแผนกแบบย่อ<br>department name shot</br></label>
					<div class="col-sm-10">
						<input type="text" class="form-control" name="dep_short_name" required>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-2 col-sm-2 control-label">อีเมล<br>email</br></label>
					<div class="col-sm-10">
						<input type="email" class="form-control" name="dep_email" required>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-2 col-sm-2 control-label">เบอร์โทรศัพท์<br>number phone</br></label>
					<div class="col-sm-10">
						<input type="text" class="form-control" name="dep_tel" required>
					</div>
				</div>
				<div class="radio">
					<h4 class="mb"><i class="fa fa-angle-right"></i> สถานะแผนก</h4><br><h5>department status</h5></br>
					<label>
						<input type="radio" name="dep_status" id="radio_active" value="active" checked>
						Active
					</label>
				</div>
				<div class="radio">
					<label>
						<input type="radio" name="dep_status" id="radio_inactive" value="inactive">
						Inactive
					</label>
				</div>
				<div class="form-group">
					<div class="col-sm-12 text-right">
						<input type="submit" class="btn btn-success" value="เพิ่มรายการ">
						<!-- HIDDEN DATA -->
						<input type="hidden" name="_token" value="{{csrf_token()}}">
					</div>
				</div>
			</form>
		</div>
	</div><!-- col-lg-12-->
</div>
	@if(isset($result))
	<div class="col-md-12 mt">
      	<div class="content-panel">
			<table class="table table-hover" id="tb_document">
				<h4><i class="fa fa-angle-right"></i> รายการแผนก</h4>
				<hr>
				<thead>
					<tr>
						<th>#</th>
						<th>Name</th>
						<th>Email</th>
						<th>Status</th>
					</tr>
				</thead>
				<tbody>
					@php($count = 0)
			@foreach($result as $depart)
					<tr>
						<td>{{++$count}}</td>
						<td>{{$depart->dep_long_name}}</td>
 						<td>{{$depart->dep_email}}</td>
						<td>{{$depart->dep_status}}</td>
					</tr>
					@endforeach
				</tbody>
			</table>
      	  </div><!-- content-panel -->
      </div><!-- /col-md-12 -->
    @else
    	<h2>DATA NOT FOUND</h2>
	@endif
@endsection

@section('onready_section')
<script>
	$(document).ready(function() {
		$("#tb_document").DataTable();
	});
</script>
@endsection