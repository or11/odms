@extends('dashgum.layout.index_layout')
@section('title_site', "การค้นหา")
@section('header_title', "ODMS")
@section('content_section')
@php
	$url = Auth::user()->isUser() ? "view" : "detail";
@endphp
<h2>@yield('title_site')</h2>
<div class="row mt">
	<div class="col-lg-12">
		<div class="form-panel">
			<h4 class="mb"><i class="fa fa-angle-right"></i> ค้นหาเอกสาร</h4>
  			<form class="form-horizontal style-form" method="post" action="{{url('doc/search')}}">
				<div class="form-group">
					<label class="col-sm-2 col-sm-2 control-label">เลขที่เอกสาร,เลขที่นำเข้า<br>document id</br></label>  
  					<div class="col-sm-10">
						<input type="text" class="form-control" name="dt_doc_code" required>
					</div>	
				</div>
				<div class="form-group">
					<div class="col-sm-2 control-label">ประเภทเอกสาร<br>category</div>
					<div class="col-sm-10">
						<div class="radio">
						  <label>
						    <input type="radio" name="doc_type" value="all" checked>
						    ทุกประเภท
						  </label>
						</div>
						<div class="radio">
						  <label>
						    <input type="radio" name="doc_type" value="msds">
						    MSDS
						  </label>
						</div>
						<div class="radio">
						  <label>
						    <input type="radio" name="doc_type" value="mi">
						    MI
						  </label>
						</div>
						<div class="radio">
						  <label>
						    <input type="radio" name="doc_type" value="manual">
						    Manual
						  </label>
						</div>
						<div class="radio">
						  <label>
						    <input type="radio" name="doc_type" value="standard">
						    Standard
						  </label>
						</div>
					</div>
				</div>
				<div class="form-group">
					<div class="col-sm-12 text-right">
						<button type="submit" class="btn btn-success">ค้นหา</button>
						<!-- HIDDEN DATA -->
						<input type="hidden" name="_token" value="{{csrf_token()}}">
					</div>
				</div>
			</form>
		</div>
	</div><!-- col-lg-12-->
</div>

<!-- MSDS -->
@if(isset($msds) && sizeof($msds)>0)
<div class="row mt">
	<div class="col-lg-12">
		<div class="form-panel">
			<table class="table table-hover" id="tb_document">
				<h4 class="mb"><i class="fa fa-angle-right"></i> MSDS</h4>
				<hr>
				<thead>
					<tr>
						<th>#</th>
						<th>เลขที่เอกสาร</th>
						<th>เลขที่รับ</th>
						<th>บริษัท</th>
						<th>วันที่</th>
						<th>รายละเอียด</th>
					</tr>
				</thead>

				<tbody>
			@php($count = 0)
			@foreach($msds as $doc)
					<tr>
						<td>{{++$count}}</td>
						<td>{{$doc->msds_doc_code}}</td>
						<td>{{$doc->drec_receive_code}}</td>
						<td>{{$doc->msds_company_name}}</td>
						<td>{{$doc->msds_doc_date}}</td>
						<td>
							<a href="{{url($url.'/msds-'.$doc->msds_doc_code)}}" class="btn btn-success btn-xs">รายละเอียด</a>
						</td>
					</tr>
			@endforeach
				</tbody>
			</table>
      	  </div><!-- content-panel -->
      </div><!-- /col-md-12 -->
</div>
@endif
<!-- MSDS -->
<!-- MI -->
@if(isset($mi) && sizeof($mi)>0)
<div class="row mt">
	<div class="col-lg-12">
		<div class="form-panel">
			<table class="table table-hover" id="tb_document">
				<h4 class="mb"><i class="fa fa-angle-right"></i> MI</h4>
				<hr>
				<thead>
					<tr>
						<th>#</th>
						<th>เลขที่เอกสาร</th>
						<th>เลขที่รับ</th>
						<th>ชื่อเอกสาร</th>
						<th>ดีไซเนอร์</th>
						<th>วันที่</th>
						<th>รายละเอียด</th>
					</tr>
				</thead>

				<tbody>
			@php($count = 0)
			@foreach($mi as $doc)
					<tr>
						<td>{{++$count}}</td>
						<td>{{$doc->mi_doc_code}}</td>
						<td>{{$doc->drec_receive_code}}</td>
						<td>{{$doc->mi_doc_name}}</td>
						<td>{{$doc->mi_designer}}</td>
						<td>{{$doc->mi_doc_date}}</td>
						<td>
							<a href="{{url($url.'/mi-'.$doc->mi_doc_code)}}" class="btn btn-success btn-xs">รายละเอียด</a>
						</td>
					</tr>
			@endforeach
				</tbody>
			</table>
      	  </div><!-- content-panel -->
      </div><!-- /col-md-12 -->
</div>
@endif
<!-- MI -->
<!-- Standard -->
@if(isset($standard) && sizeof($standard)>0)
<div class="row mt">
	<div class="col-lg-12">
		<div class="form-panel">
			<table class="table table-hover" id="tb_document">
				<h4 class="mb"><i class="fa fa-angle-right"></i> Standard</h4>
				<hr>
				<thead>
					<tr>
						<th>#</th>
						<th>เลขที่เอกสาร</th>
						<th>เลขที่รับ</th>
						<th>ชื่อเอกสาร</th>
						<th>ประเภทเอกสาร</th>
						<th>วันที่</th>
						<th>รายละเอียด</th>
					</tr>
				</thead>

				<tbody>
			@php($count = 0)
			@foreach($standard as $doc)
					<tr>
						<td>{{++$count}}</td>
						<td>{{$doc->standard_doc_code}}</td>
						<td>{{$doc->drec_receive_code}}</td>
						<td>{{$doc->standard_doc_name}}</td>
						<td>{{$doc->dt_long_name}}</td>
						<td>{{$doc->standard_doc_date}}</td>
						<td>
							<a href="{{url($url.'/standard-'.$doc->standard_doc_code)}}" class="btn btn-success btn-xs">รายละเอียด</a>
						</td>
					</tr>
			@endforeach
				</tbody>
			</table>
      	  </div><!-- content-panel -->
      </div><!-- /col-md-12 -->
</div>
@endif
<!-- Standard -->
<!-- Manual -->
@if(isset($manual) && sizeof($manual)>0)
<div class="row mt">
	<div class="col-lg-12">
		<div class="form-panel">
			<table class="table table-hover" id="tb_document">
				<h4 class="mb"><i class="fa fa-angle-right"></i> Manual</h4>
				<hr>
				<thead>
					<tr>
						<th>#</th>
						<th>เลขที่เอกสาร</th>
						<th>เลขที่รับ</th>
						<th>ชื่อเครื่องจักร</th>
						<th>แผนก</th>
						<th>วันที่</th>
						<th>รายละเอียด</th>
					</tr>
				</thead>

				<tbody>
			@php($count = 0)
			@foreach($manual as $doc)
					<tr>
						<td>{{++$count}}</td>
						<td>{{$doc->manual_doc_code}}</td>
						<td>{{$doc->drec_receive_code}}</td>
						<td>{{$doc->manual_machine_name}}</td>
						<td>{{$doc->dep_long_name}}</td>
						<td>{{$doc->manual_doc_date}}</td>
						<td>
							<a href="{{url($url.'/manual-'.$doc->manual_doc_code)}}" class="btn btn-success btn-xs">รายละเอียด</a>
						</td>
					</tr>
			@endforeach
				</tbody>
			</table>
      	  </div><!-- content-panel -->
      </div><!-- /col-md-12 -->
</div>
@endif
<!-- Manual -->
@endsection

@section('onready_section')
<script>
	$(document).ready(function() {
		// $("#tb_document").DataTable();
	});
</script>
@endsection