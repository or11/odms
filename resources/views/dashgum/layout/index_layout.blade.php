<!DOCTYPE html>
<html lang="en">
  <head>
    
    @include('dashgum.component.meta')
    @yield('meta')
    
    @include('dashgum.css.css')
    @yield('stylesheet')
    
  </head>

  <body>
  <section id="container" >
      <!-- **********************************************************************************************************************************************************
      TOP BAR CONTENT & NOTIFICATIONS
      *********************************************************************************************************************************************************** -->
      <!--header start-->
      <header class="header black-bg">
        @include('dashgum.component.header')
        @yield('header')
      </header>
      <!--header end-->
      
      <!-- **********************************************************************************************************************************************************
      MAIN SIDEBAR MENU
      *********************************************************************************************************************************************************** -->
      <!--sidebar start-->
      <aside>
        @include('dashgum.component.left_side')
        @yield('left_side')
      </aside>
      <!--sidebar end-->
      
      <!-- **********************************************************************************************************************************************************
      MAIN CONTENT
      *********************************************************************************************************************************************************** -->
      <!--main content start-->
      <section id="main-content">
          <section class="wrapper">
            @include('dashgum.component.content')
            @yield('content')
          </section>
      </section>

      <!--main content end-->
      <!--footer start-->
      <footer class="site-footer">
          <div class="text-center">
              2014 - Alvarez.is
              <a href="{{url('#')}}" class="go-top">
                  <i class="fa fa-angle-up"></i>
              </a>
          </div>
      </footer>
      <!--footer end-->
  </section>

    @include('dashgum.js.javascript')
    @yield('javascript')
	
  	@include('dashgum.js.custom_script')
    @yield('custom_script')

    @yield('onready_section')

  </body>
</html>