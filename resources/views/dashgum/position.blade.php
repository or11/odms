@php
	$pagename = "ตำแหน่งงาน"
@endphp
@extends('dashgum.layout.index_layout')
@section('title_site', $pagename)
@section('header_title', "ODMS")


@section('content_section')
<h2>{{$pagename}}</h2>
<div class="row mt">
	<div class="col-lg-12">
		<div class="form-panel">
			<h4 class="mb"><i class="fa fa-angle-right"></i> ข้อมูลตำแหน่งงาน</h4>
			<form class="form-horizontal style-form" method="post" action="{{url('master-data/position')}}">
				<div class="form-group">
					<label class="col-sm-2 col-sm-2 control-label">ชื่อตำแหน่งงาน<br>position name</br></label>
					<div class="col-sm-10">
						<input type="text" class="form-control" name="pos_long_name" required>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-2 col-sm-2 control-label">ชื่อตำแหน่งแบบย่อ<br>position name shot</br></label>
					<div class="col-sm-10">
						<input type="text" class="form-control" name="pos_short_name" required>
					</div>
				</div>
				<div class="radio">
					<h4 class="mb"><i class="fa fa-angle-right"></i> สถานะตำแหน่งงาน</h4><br><h5>position status</h5></br>
					<label>
						<input type="radio" name="pos_status" id="radio_active" value="active" checked>
						Active
					</label>
				</div>
				<div class="radio">
					<label>
						<input type="radio" name="pos_status" id="radio_inactive" value="inactive">
						Inactive
					</label>
				</div>
				<div class="form-group">
					<div class="col-sm-12 text-right">
						<input type="submit" class="btn btn-success" value="เพิ่มรายการ">
						<!-- HIDDEN DATA -->
						<input type="hidden" name="_token" value="{{csrf_token()}}">
					</div>
				</div>
			</form>
		</div>
	</div><!-- col-lg-12-->
</div>
	@if(isset($result))
	<div class="col-md-12 mt">
      	<div class="content-panel">
			<table class="table table-hover" id="tb_document">
				<h4><i class="fa fa-angle-right"></i> รายการตำแหน่งงาน</h4>
				<hr>
				<thead>
					<tr>
						<th>#</th>
						<th>Position</th>
						<th>Status</th>
					</tr>
				</thead>
				<tbody>
				@php($count = 0)
				@foreach($result as $position)
					<tr>
						<td>{{++$count}}</td>
						<td>{{$position->pos_short_name}} <br> <small>{{$position->pos_long_name}}</small></td>
						<td>{{$position->pos_status}}</td>
					</tr>
				@endforeach
				</tbody>
			</table>
      	  </div><!-- content-panel -->
      </div><!-- /col-md-12 -->
    @else
    	<h2>DATA NOT FOUND</h2>
	@endif
@endsection

@section('onready_section')
<script>
	$(document).ready(function() {
		$("#tb_document").DataTable();
	});
</script>
@endsection