@extends('dashgum.layout.index_layout')
@section('title_site', "รายละเอียด")
@section('header_title', "ODMS")



@section('content_section')
<h2>@yield('title_site')</h2>
<!-- Manual -->
@if(isset($manual) && sizeof($manual)>0)
@foreach($manual as $m)
	@section('bstatus')
		@if(isset($bstatus) && $bstatus)
			<a href="{{url('make/borrow/manual-'.$m->manual_doc_code)}}" 
					class="btn btn-lg btn-success">ยืมเอกสาร</a>
		@else
			<h4>สถานะยังไม่สามารถทำการยืมได้</h4>
		@endif
	@endsection
	@section('file', url('make/file-borrow/manual-'.base64_encode($m->manual_doc_code)))
<div class="row mt">
	<div class="col-lg-12">
		<div class="showback">
			<h4 class="mb"><i class="fa fa-angle-right"></i> Manual</h4>
			<div class="row mb">
				<div class="col-md-6 text-right">เลขที่เอกสาร</div>
				<div class="col-md-6 text-left">{{$m->manual_doc_code}}</div>
			</div>
			<div class="row mb">
				<div class="col-md-6 text-right">ชื่อเครื่อง</div>
				<div class="col-md-6 text-left">{{$m->manual_machine_name}}</div>
			</div>
			<div class="row mb">
				<div class="col-md-6 text-right">แผนก</div>
				<div class="col-md-6 text-left">
					{{$m->dep_long_name}} <br>
					<a href="mailto:{{$m->dep_email}}"><small>{{$m->dep_email}}</small></a>
				</div>
			</div>
			<div class="row mb">
				<div class="col-md-6 text-right">วันที่ในเอกสาร</div>
				<div class="col-md-6 text-left">{{$m->manual_doc_date}}</div>
			</div>
			<div class="row mb">
				<div class="col-md-6 text-right">เลขที่รับ</div>
				<div class="col-md-6 text-left">{{$m->drec_receive_code}}</div>
			</div>
			<div class="row mb">
				<div class="col-md-6 text-right">วันรับเข้าเอกสาร</div>
				<div class="col-md-6 text-left">{{$m->drec_date}}</div>
			</div>
			<div class="row mb">
				<div class="col-md-6 text-right">สถานที่เก็บ</div>
				<div class="col-md-6 text-left">{{$m->drec_store_place}}</div>
			</div>
			<div class="row mb">
				<div class="col-md-6 text-right">ไฟล์</div>
				<div class="col-md-6 text-left">
					<a href="@yield('file')">
						<img src="{{asset('dashgum/img/pdf-icon.png')}}">
					</a>
				</div>
			</div>
			<div class="row mb">
				<div class="col-md-12 text-center">
					@yield('bstatus')
				</div>
			</div>
		</div>
	</div><!-- col-lg-12-->
</div>
@endforeach
@endif
<!-- Manual -->

<!-- MSDS -->
@if(isset($msds) && sizeof($msds)>0)
@foreach($msds as $m)
	@section('bstatus')
		@if(isset($bstatus) && $bstatus)
			<a href="{{url('make/borrow/msds-'.$m->msds_doc_code)}}" 
					class="btn btn-lg btn-success">ยืมเอกสาร</a>
		@else
			<h4>สถานะยังไม่สามารถทำการยืมได้</h4>
		@endif
	@endsection
	@section('file', url('make/file-borrow/msds-'.base64_encode($m->msds_doc_code)))
<div class="row mt">
	<div class="col-lg-12">
		<div class="showback">
			<h4 class="mb"><i class="fa fa-angle-right"></i> msds</h4>
			<div class="row mb">
				<div class="col-md-6 text-right">เลขที่เอกสาร</div>
				<div class="col-md-6 text-left">{{$m->msds_doc_code}}</div>
			</div>
			<div class="row mb">
				<div class="col-md-6 text-right">วันที่ในเอกสาร</div>
				<div class="col-md-6 text-left">{{$m->msds_doc_date}}</div>
			</div>
			<div class="row mb">
				<div class="col-md-6 text-right">ชื่อสารเคมี</div>
				<div class="col-md-6 text-left">{{$m->msds_chem_name}}</div>
			</div>
			<div class="row mb">
				<div class="col-md-6 text-right">ประเภทสารเคมี</div>
				<div class="col-md-6 text-left">{{$m->che_long_name}}</div>
			</div>
			<div class="row mb">
				<div class="col-md-6 text-right">บริษัท</div>
				<div class="col-md-6 text-left">{{$m->msds_company_name}}</div>
			</div>
			<div class="row mb">
				<div class="col-md-6 text-right">เลขที่รับ</div>
				<div class="col-md-6 text-left">{{$m->drec_receive_code}}</div>
			</div>
			<div class="row mb">
				<div class="col-md-6 text-right">วันรับเข้าเอกสาร</div>
				<div class="col-md-6 text-left">{{$m->drec_date}}</div>
			</div>
			<div class="row mb">
				<div class="col-md-6 text-right">สถานที่เก็บ</div>
				<div class="col-md-6 text-left">{{$m->drec_store_place}}</div>
			</div>
			<div class="row mb">
				<div class="col-md-6 text-right">ไฟล์</div>
				<div class="col-md-6 text-left">
					<a href="@yield('file')">
						<img src="{{asset('dashgum/img/pdf-icon.png')}}">
					</a>
				</div>
			</div>
			<div class="row mb">
				<div class="col-md-12 text-center">
					@yield('bstatus')
				</div>
			</div>
		</div>
	</div><!-- col-lg-12-->
</div>
@endforeach
@endif
<!-- MSDS -->

<!-- Standard -->
@if(isset($standard) && sizeof($standard)>0)
@foreach($standard as $m)
	@section('bstatus')
		@if(isset($bstatus) && $bstatus)
			<a href="{{url('make/borrow/standard-'.$m->standard_doc_code)}}" 
					class="btn btn-lg btn-success">ยืมเอกสาร</a>
		@else
			<h4>สถานะยังไม่สามารถทำการยืมได้</h4>
		@endif
	@endsection
	@section('file', url('make/file-borrow/standard-'.base64_encode($m->standard_doc_code)))
<div class="row mt">
	<div class="col-lg-12">
		<div class="showback">
			<h4 class="mb"><i class="fa fa-angle-right"></i> standard</h4>
			<div class="row mb">
				<div class="col-md-6 text-right">เลขที่เอกสาร</div>
				<div class="col-md-6 text-left">{{$m->standard_doc_code}}</div>
			</div>
			<div class="row mb">
				<div class="col-md-6 text-right">วันที่ในเอกสาร</div>
				<div class="col-md-6 text-left">{{$m->standard_doc_date}}</div>
			</div>
			<div class="row mb">
				<div class="col-md-6 text-right">ชื่อเอกสาร</div>
				<div class="col-md-6 text-left">{{$m->standard_doc_name}}</div>
			</div>
			<div class="row mb">
				<div class="col-md-6 text-right">ประเภทเอกสาร</div>
				<div class="col-md-6 text-left">{{$m->dt_long_name}}</div>
			</div>
			<div class="row">
				<div class="col-md-12 text-center">รายละเอียด</div>
			</div>
			<div class="row mb">
				<div class="col-md-1"></div>
				<div class="col-md-10 text-left">{{$m->standard_description}}</div>
				<div class="col-md-1"></div>
			</div>
			<div class="row mb">
				<div class="col-md-6 text-right">เลขที่รับ</div>
				<div class="col-md-6 text-left">{{$m->drec_receive_code}}</div>
			</div>
			<div class="row mb">
				<div class="col-md-6 text-right">วันรับเข้าเอกสาร</div>
				<div class="col-md-6 text-left">{{$m->drec_date}}</div>
			</div>
			<div class="row mb">
				<div class="col-md-6 text-right">สถานที่เก็บ</div>
				<div class="col-md-6 text-left">{{$m->drec_store_place}}</div>
			</div>
			<div class="row mb">
				<div class="col-md-6 text-right">ไฟล์</div>
				<div class="col-md-6 text-left">
					<a href="@yield('file')">
						<img src="{{asset('dashgum/img/pdf-icon.png')}}">
					</a>
				</div>
			</div>
			<div class="row mb">
				<div class="col-md-12 text-center">
					@yield('bstatus')
				</div>
			</div>
		</div>
	</div><!-- col-lg-12-->
</div>
@endforeach
@endif
<!-- Standard -->

<!-- MI -->
@if(isset($mi) && sizeof($mi)>0)
@foreach($mi as $m)
	@section('bstatus')
		@if(isset($bstatus) && $bstatus)
			<a href="{{url('make/borrow/mi-'.$m->mi_doc_code)}}" 
					class="btn btn-lg btn-success">ยืมเอกสาร</a>
		@else
			<h4>สถานะยังไม่สามารถทำการยืมได้</h4>
		@endif
	@endsection
	@section('file', url('make/file-borrow/mi-'.base64_encode($m->mi_doc_code)))
<div class="row mt">
	<div class="col-lg-12">
		<div class="showback">
			<h4 class="mb"><i class="fa fa-angle-right"></i> mi</h4>
			<div class="row mb">
				<div class="col-md-6 text-right">เลขที่เอกสาร</div>
				<div class="col-md-6 text-left">{{$m->mi_doc_code}}</div>
			</div>
			<div class="row mb">
				<div class="col-md-6 text-right">รหัสเอกสาร</div>
				<div class="col-md-6 text-left">{{$m->mi_doc_no}}</div>
			</div>
			<div class="row mb">
				<div class="col-md-6 text-right">ชื่อเอกสาร</div>
				<div class="col-md-6 text-left">{{$m->mi_doc_name}}</div>
			</div>
			<div class="row mb">
				<div class="col-md-6 text-right">ดีไซเนอร์</div>
				<div class="col-md-6 text-left">{{$m->mi_designer}}</div>
			</div>
			<div class="row">
				<div class="col-md-12 text-center">รายละเอียด</div>
			</div>
			<div class="row mb">
				<div class="col-md-1"></div>
				<div class="col-md-10 text-left">{{$m->mi_description}}</div>
				<div class="col-md-1"></div>
			</div>
			<div class="row mb">
				<div class="col-md-6 text-right">เลขที่รับ</div>
				<div class="col-md-6 text-left">{{$m->drec_receive_code}}</div>
			</div>
			<div class="row mb">
				<div class="col-md-6 text-right">วันรับเข้าเอกสาร</div>
				<div class="col-md-6 text-left">{{$m->drec_date}}</div>
			</div>
			<div class="row mb">
				<div class="col-md-6 text-right">สถานที่เก็บ</div>
				<div class="col-md-6 text-left">{{$m->drec_store_place}}</div>
			</div>
			<div class="row mb">
				<div class="col-md-6 text-right">ไฟล์</div>
				<div class="col-md-6 text-left">
					<a href="@yield('file')">
						<img src="{{asset('dashgum/img/pdf-icon.png')}}">
					</a>
				</div>
			</div>
			<div class="row mb">
				<div class="col-md-12 text-center">
					@yield('bstatus')
				</div>
			</div>
		</div>
	</div><!-- col-lg-12-->
</div>
@endforeach
@endif
<!-- MI -->
@endsection

@section('onready_section')
<script>
	$(document).ready(function() {
		// $("#tb_document").DataTable();
	});
</script>
@endsection