@php
	$pagename = "เอกสาร MI"
@endphp
@extends('dashgum.layout.index_layout')
@section('title_site', $pagename)
@section('header_title', "ODMS")


@section('content_section')
<h2>{{$pagename}}</h2>
<div class="row mt">
	<div class="col-lg-12">
		<form class="form-horizontal style-form" method="post" action="{{url('doc/receive/mi')}}"
			enctype="multipart/form-data">
			<div class="form-panel">
				<h4 class="mb"><i class="fa fa-angle-right"></i> ข้อมูลเอกสาร MI</h4>
				<div class="form-group">
					<label class="col-sm-2 col-sm-2 control-label">รหัสเอกสาร<br>document id</br></label>
					<div class="col-sm-10">
						<input type="text" class="form-control" name="mi_doc_code" required>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-2 col-sm-2 control-label">เลขที่เอกสาร<br>document number</br></label>
					<div class="col-sm-10">
						<input type="text" class="form-control" name="mi_doc_no" required>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-2 col-sm-2 control-label">วันที่ในเอกสาร<br>document date</br></label>
					<div class="col-sm-10">
						<input type="date" class="form-control" name="mi_doc_date" required>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-2 col-sm-2 control-label">ชื่อเอกสาร<br>document name</br></label>
					<div class="col-sm-10">
						<input type="text" class="form-control" name="mi_doc_name" required>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-2 col-sm-2 control-label">รายละเอียด<br>description</br></label>
					<div class="col-sm-10">
						<textarea name="mi_description" id="" cols="30" rows="10" class="form-control"></textarea>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-2 col-sm-2 control-label">ชื่อผู้ออกแบบ<br>creator name</br></label>
					<div class="col-sm-10">
						<input type="text" class="form-control" name="mi_designer" required>
					</div>
				</div>
				<div class="radio">
					<h4 class="mb"><i class="fa fa-angle-right"></i>สถานะเอกสาร</h4><br><h5>document status</h5>
					<label>
						<input type="radio" name="mi_status" id="radio_active" value="active" checked>
						Active
					</label>
				</div>
				<div class="radio">
					<label>
						<input type="radio" name="mi_status" id="radio_inactive" value="inactive">
						Inactive
					</label>
				</div>
			</div> <!-- form-panel -->
		<p><br></p> 
			<!-- RECEIVE FORM -->
		<div class="form-panel">
			<h4 class="mb"><i class="fa fa-angle-right"></i> การนำเข้า</h4>
				<div class="form-group">
					<label class="col-sm-2 col-sm-2 control-label">เลขที่รับ<br>no receive</br></label>
					<div class="col-sm-10">
						<input type="text" class="form-control" name="drec_receive_code" required>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-2 col-sm-2 control-label">สถานที่จัดเก็บเอกสาร<br>document address</br></label>
					<div class="col-sm-10">
						<input type="text" class="form-control" name="drec_store_place" required>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-2 col-sm-2 control-label">ไฟล์<br>file</br></label>
					<div class="col-sm-10">
						<input type="file" class="form-control" name="drec_file_path"
							accept="application/pdf" required>
					</div>
				</div>
				<div class="form-group">
					<div class="col-sm-12 text-right">
						<input type="submit" class="btn btn-success" value="เพิ่มรายการ">
						<!-- HIDDEN DATA -->
						<input type="hidden" name="_token" value="{{csrf_token()}}">
						<input type="hidden" name="drec_doc_table" value="mi">
						<input type="hidden" name="receive_status" value="available">
						<input type="hidden" name="subd_doc_master" value="">
					</div>
				</div>
			</div> <!-- form-panel -->
		</form>
	</div><!-- col-lg-12-->
</div>
	@if(isset($result))
	<div class="col-md-12 mt">
      	<div class="content-panel">
			<table class="table table-hover" id="tb_document">
				<h4><i class="fa fa-angle-right"></i> รายการตำแหน่งงาน</h4>
				<hr>
				<thead>
					<tr>
						<th>#</th>
						<th>Position</th>
						<th>Status</th>
					</tr>
				</thead>
				<tbody>
				@php($count = 0)
				@foreach($result as $position)
					<tr>
						<td>{{++$count}}</td>
						<td>{{$position->pos_short_name}} <br> <small>{{$position->pos_long_name}}</small></td>
						<td>{{$position->pos_status}}</td>
					</tr>
				@endforeach
				</tbody>
			</table>
      	  </div><!-- content-panel -->
      </div><!-- /col-md-12 -->
	@endif
@endsection

@section('onready_section')
<script>
	$(document).ready(function() {
		$("#tb_document").DataTable();
	});
</script>
@endsection