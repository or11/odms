@extends('dashgum.layout.index_layout')
@section('title_site', "")
@section('header_title', "ODMS")

 
@section('content_section')
<div class="row mt">
	<div class="col-lg-12">
		<div class="form-panel">
			<h4 class="mb">ประวัติของบริษัท</h4><br>
			<center>
			<div><img src="dashgum/img/banner.jpg" alt="HTML5 Icon" style="width:750px;height:200px;"></div>
			</center>
			<br><br>
    <blockquote><h5>บริษัท เคซีอี อีเลคโทรนิคส์ จำกัด ( มหาชน ) เดิมชื่อ บริษัท ควงเจริญ อีเลคโทรนิคส์ จำกัด สำนักงานและโรงงานxตั้งอยู่เลขที่ 72-72/1-3 นิคมอุตสาหกรรมลาดกระบัง ชอยฉลองกรุง 31 แขวงลำปลาทิว เขตลาดกระบัง กรุงเทพฯ โทรศัพท์ (662) 326-0196 โทรสาร (662) 326-0300

เคซีอีฯ เริ่มก่อตั้งในปี 2525 ได้เข้าจดทะเบียนในตลาดหลักทรัพย์แห่งประเทศไทยในปี 2531 และแปรสภาพเป็นบริษัทมหาชนในปี 2535 ปัจจุบันมีทุนจดทะเบียน 500 ล้านบาท เรียกชำระแล้วรวม 462.497 ล้านบาท เคซีอีฯ เป็นผู้ผลิตและจำหน่ายแผ่นพิมพ์วงจรอิเล็กทรอนิกส์ หรือ PCB (PRINTED CIRCUIT BOARD) ซึ่งเป็นแผ่น EPOXY GLASS ที่มีสื่อนำไฟฟ้า เช่น ตะกั่ว ทองแดง เคลือบอยู่ภายใต้เครื่องหมายการค้า “KCE” ผลิตภัณฑ์ดังกล่าวเป็นชิ้นส่วนสำคัญขั้นพื้นฐานในการประกอบเครื่องคอมพิวเตอร์ เครื่องมือสื่อสาร โทรคมนาคม เครื่องมืออิเล็กทรอนิกส์เกือบทุกประเภท</h5></blockquote>
			</br><br>
			<h4>ลักษณะผลิตภัณฑ์</h4>
			<br> <blockquote><h5> PCB เป็นส่วนประกอบสำคัญของอุปกรณ์อิเล็กทรอนิกส์ทุกประเภท อุปกรณ์ไฟฟ้าทุกชิ้นที่ใช้อยู่ในชีวิตประจำวันจะต้องมีแผ่น PCB อย่างน้อย 1 ชิ้น หรือมากกว่า PCB เป็นส่วนประกอบพื้นฐานของวงจรอิเล็กทรอนิกส์ ลักษณะเป็นแผ่นที่มีลายวงจรทองแดงเป็นทางเดินสัญญาณไฟฟ้าอยู่ ใช้สำหรับต่อวางอุปกรณ์อิเล็กทรอนิกส์เพื่อประกอบเป็นวงจรแทนการต่อวงจรด้วยสายไฟซึ่งมีความซับซ้อนและยุ่งยาก

PCB ประกอบด้วยส่วนสำคัญ 2 ส่วนคือ แผ่นฐาน หรือซับสเต็รท (substrate) กับส่วนที่เป็นตัวนำ แผ่นฐานเป็นฉนวนบางๆทำหน้าที่เป็นฐานที่วางและยึดติดตัวอุปกรณ์ โดยมีตัวนำไฟฟ้าที่เป็นทองแดงเป็นตัวต่อวงจรให้แก่อุปกรณ์ แผ่นฐานจะทำจากวัสดุอีพ็อกซีไฟเบอร์กลาส ซึ่งเป็นการทอใยแก้วเข้าด้วยกันแล้วยึดด้วยอีพ็อกซีเรซิ่น มีคุณสมบัติดี สามารถใช้งานได้กับงานหลายประเภทเพราะทนต่อความชื้นและอุณหภูมิสูง ไม่บิดงอได้ง่าย แผ่นฐานมักเป็นสีเขียวหรือสีฟ้า

แผ่นวงจรนี้อาจทำเพียงหน้าเดียว หรือสองหน้า แต่ถ้าวงจรมีความหนาสูงและมีความซับซ้อนมากๆก็อาจจะต้องทำเป็นหลายๆชั้นก็ได้ ตามความต้องการของผู้ออกแบบ

PCB ชนิดหน้าเดียว (Single sided) มีเส้นลายวงจรเพื่อเชื่อมสัญญาณไฟฟ้าอยู่เพียงด้านเดียว
PCB ชนิดสองหน้าเคลือบรู (Double sided Plated-Through Hole) มีเส้นลายวงจรเพื่อเชื่อมสัญญาณ ไฟฟ้าอยู่ทั้งสองด้าน มีการเจาะรูบนแผ่นฐานเพื่อเป็นที่ไว้สอดขาอุปกรณ์ และมีการเคลือบผิวภายในรูด้วยทองแดง เพื่อเชื่อมต่อสัญญาณไฟฟ้าระหว่างด้านหน้าและด้านหลังของแผ่นบอร์ด
PCB ชนิดหลายชั้น (Multilayer) มีเส้นลายวงจรเพื่อเชื่อมสัญญาณไฟฟ้าระหว่างชั้นใน (Inner Layer) และชั้นนอก (Outer Layer)
ทั้งนี้ PCB ที่บริษัทฯ เป็นผู้ผลิต มีอยู่ 2 แบบ คือ

การผลิตแบบ 2 หน้าเคลือบรู (Double sided PLATED-THROUGH HOLES PCB) และ
การผลิตแบบหลายชั้น (Multilayer PCB) ซึ่งมีความซับซ้อนกว่า และใช้เทคโนโลยีสูงกว่าการผลิตแผ่นพิมพ์วงจรชนิด 2 หน้า ซึ่งในปัจจุบัน บริษัทฯ มีความสามารถในการผลิตได้ตั้งแต่ 4 ชั้น ถึง 24 ชั้น
แผ่นพิมพ์วงจรอิเล็กทรอนิกส์ทั้งสองแบบนี้ถูกนำไปใช้ในอุตสาหกรรมต่างๆ เช่น ในอุตสาหกรรมยานยนต์ อุตสาหกรรมผลิตภัณฑ์ที่ใช้ในอุตสาหกรรม อุปกรณ์โทรคมนาคมต่างๆ รวมถึง เครื่องมือทางการแพทย์ โทรศัพท์มือถือ และอุปกรณ์คอมพิวเตอร์ เป็นต้น

เนื่องจากในการผลิต PCB ต้องใช้เทคโนโลยีที่ทันสมัย มีเทคนิคพิเศษเฉพาะ และต้องได้มาตรฐานคุณภาพที่เป็นที่ยอมรับของอุตสาหกรรม บริษัทฯ จึงต้องมีการพัฒนาคุณภาพ เทคโนโลยีการผลิตและทักษะของพนักงานผลิตอยู่ตลอดเวลา บริษัทฯได้ขยายโรงงานเพื่อเพิ่มกำลังการผลิตอย่างต่อเนื่อง และได้แต่งตั้งตัวแทนจำหน่ายในต่างประเทศหลายแห่ง เพื่อตอบสนองความต้องการ (Demand) ในผลิตภัณฑ์ PCB ที่เติบโตขึ้นอย่างรวดเร็วในอุตสาหกรรมอิเล็กทรอนิกส์ของโลก</blockquote></h5>
			<!-- <form class="form-horizontal style-form" method="get">
				<div class="form-group">
					<label class="col-sm-2 col-sm-2 control-label">Default</label>
					<div class="col-sm-10">
						<input type="text" class="form-control">
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-2 col-sm-2 control-label">Help text</label>
					<div class="col-sm-10">
						<input type="text" class="form-control">
						<span class="help-block">A block of help text that breaks onto a new line and may extend beyond one line.</span>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-2 col-sm-2 control-label">Rounder</label>
					<div class="col-sm-10">
						<input type="text" class="form-control round-form">
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-2 col-sm-2 control-label">Input focus</label>
					<div class="col-sm-10">
						<input class="form-control" id="focusedInput" type="text" value="This is focused...">
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-2 col-sm-2 control-label">Disabled</label>
					<div class="col-sm-10">
						<input class="form-control" id="disabledInput" type="text" placeholder="Disabled input here..." disabled>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-2 col-sm-2 control-label">Placeholder</label>
					<div class="col-sm-10">
						<input type="text"  class="form-control" placeholder="placeholder">
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-2 col-sm-2 control-label">Password</label>
					<div class="col-sm-10">
						<input type="password"  class="form-control" placeholder="">
					</div>
				</div>
				<div class="form-group">
					<label class="col-lg-2 col-sm-2 control-label">Static control</label>
					<div class="col-lg-10">
						<p class="form-control-static">email@example.com</p>
					</div>
				</div>
			</form> -->
		</div>
		</div><!-- col-lg-12-->
	</div>
@endsection