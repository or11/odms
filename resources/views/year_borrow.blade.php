<!DOCTYPE html>
<html lang="en">
<head>
	<!-- <meta charset="UTF-8"> -->
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/><!-- Bootstrap core CSS -->
    
	<style>
		@font-face {
            font-family: 'THSarabunNew';
            font-style: normal;
            font-weight: normal;
            src: url("{{ storage_path('fonts/THSarabunNew.ttf') }}") format('truetype');
        }
        @font-face {
            font-family: 'THSarabunNew';
            font-style: normal;
            font-weight: bold;
            src: url("{{ storage_path('fonts/THSarabunNew Bold.ttf') }}") format('truetype');
        }
        @font-face {
            font-family: 'THSarabunNew';
            font-style: italic;
            font-weight: normal;
            src: url("{{ storage_path('fonts/THSarabunNew Italic.ttf') }}") format('truetype');
        }
        @font-face {
            font-family: 'THSarabunNew';
            font-style: italic;
            font-weight: bold;
            src: url("{{ storage_path('fonts/THSarabunNew BoldItalic.ttf') }}") format('truetype');
        }

        body {
            font-family: "THSarabunNew";
        }
        table, tr, td, th{
            border: black solid 1px;
            border-collapse: collapse;
            font-size: 20px;
            padding: 5px 5px 5px 5px;
            line-height: 15px;
        }
        th{
            text-align: center;
            font-weight: bolder;
            font-size: 24px;
        }
        table{
            width: 100%;
        }
        .center{
            text-align: center;
        }
	</style>
	<title>รายงานการยืมเอกสารประจำปี 2560</title>
</head>
<body>
    <caption>
        <h1>รายงานการยืมเอกสารประจำปี 2560</h1>
    </caption>
  <table>
        <thead>
            <tr>
                <th>#</th>
                <th>ประเภท</th>
                <th>ชื่อ</th>
                <th>ผู้ยืม</th>
                <th>สถานะ</th>
                <th>วันที่</th>
            </tr>
        </thead>
        <tbody>
@if(isset($result) && sizeof($result)>0)
    @php($count = 0)
    @foreach($result as $r)
        @if($r->dbor_doc_table == 'msds')
            @php($name = $r->msds_chem_name)
        @elseif($r->dbor_doc_table == 'mi')
            @php($name = $r->mi_doc_name)
        @elseif($r->dbor_doc_table == 'manual')
            @php($name = $r->manual_machine_name)
        @elseif($r->dbor_doc_table == 'standard')
            @php($name = $r->standard_doc_name)
        @endif
            <tr>
                <td>{{++$count}}</td>
                <td>{{$r->dbor_doc_table}}</td>
                <td>{{$name}}</td>
                <td><strong>{{$r->usr_name}} {{$r->usr_lastname}}</strong><br><small>{{$r->email}}</small></td>
                <td>{{$r->status}}</td>
                <td>{{$r->dbor_borrow_date}}</td>
            </tr>
    @endforeach
@else
            <tr class="center">
                <td colspan="6">ไม่มีรายการ</td>
            </tr>
@endif
        </tbody>
  </table>
</body>

</html>

