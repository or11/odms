<!DOCTYPE html>
<html lang="en">
<head>
	<!-- <meta charset="UTF-8"> -->
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/><!-- Bootstrap core CSS -->
    
	<style>
		@font-face {
            font-family: 'THSarabunNew';
            font-style: normal;
            font-weight: normal;
            src: url("{{ storage_path('fonts/THSarabunNew.ttf') }}") format('truetype');
        }
        @font-face {
            font-family: 'THSarabunNew';
            font-style: normal;
            font-weight: bold;
            src: url("{{ storage_path('fonts/THSarabunNew Bold.ttf') }}") format('truetype');
        }
        @font-face {
            font-family: 'THSarabunNew';
            font-style: italic;
            font-weight: normal;
            src: url("{{ storage_path('fonts/THSarabunNew Italic.ttf') }}") format('truetype');
        }
        @font-face {
            font-family: 'THSarabunNew';
            font-style: italic;
            font-weight: bold;
            src: url("{{ storage_path('fonts/THSarabunNew BoldItalic.ttf') }}") format('truetype');
        }

        body {
            font-family: "THSarabunNew";
        }
        table, tr, td, th{
            border: black solid 1px;
            border-collapse: collapse;
            font-size: 20px;
            padding: 5px 5px 5px 5px;
            line-height: 15px;
        }
        th{
            text-align: center;
            font-weight: bolder;
            font-size: 24px;
        }
        table{
            width: 100%;
        }
	</style>
	<title>รายงานการรับเข้าเอกสารประจำปี 2560</title>
</head>
<body>
    <caption>
        <h1>รายงานการรับเข้าเอกสารประจำปี 2560</h1>
    </caption>
  <table>
        <thead>
            <tr>
                <th>#</th>
                <th>เลขที่รับ</th>
                <th>ประเภท</th>
                <th>รหัสเอกสาร</th>
                <th>ที่เก็บ</th>
                <th>วันที่</th>
            </tr>
        </thead>
        <tbody>
@if(isset($data))
    @php($count = 0)
    @foreach($data as $d)
            <tr>
                <td>{{++$count}}</td>
                <td>{{$d['rec_code']}}</td>
                <td>{{$d['type']}}</td>
                <td><strong>{{$d['doc_code']}}</strong><br><small>{{$d['doc_name']}}</small></td>
                <td>{{$d['store']}}</td>
                <td>{{$d['date']}}</td>
            </tr>
    @endforeach
@endif
        </tbody>
  </table>
</body>

</html>

