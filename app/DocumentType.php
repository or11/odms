<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DocumentType extends Model
{
    /**
     * The Table
     * @var String
     */
    protected $table = '_document_type';

    /**
     * The Primary Key
     * @var integer
     */
    protected $primaryKey = 'dt_id';


    /**
     * Time Stamps
     * @var Bool
     */
    public $timestamps = true;
}
