<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PrefixName extends Model
{
    /**
     * The Table
     * @var String
     */
    protected $table = '_prefix_name';

    /**
     * The Primary Key
     * @var integer
     */
    protected $primaryKey = 'pfn_id';


    /**
     * Time Stamps
     * @var Bool
     */
    public $timestamps = true;
}
