<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DocBorrow extends Model
{
    /**
     * The Table
     * @var String
     */
    protected $table = 'doc_borrow';

    /**
     * The Primary Key
     * @var integer
     */
    protected $primaryKey = 'dbor_id';


    /**
     * Time Stamps
     * @var Bool
     */
    public $timestamps = true;
}
