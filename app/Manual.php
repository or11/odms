<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Manual extends Model
{
    /**
     * The Table
     * @var String
     */
    protected $table = 'manual';

    /**
     * The Primary Key
     * @var integer
     */
    protected $primaryKey = 'manual_id';


    /**
     * Time Stamps
     * @var Bool
     */
    public $timestamps = true;
}
