<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ActivityLog extends Model
{
    /**
     * The Table
     * @var String
     */
    protected $table = 'activity_log';

    /**
     * The Primary Key
     * @var integer
     */
    protected $primaryKey = 'log_id';


    /**
     * Time Stamps
     * @var Bool
     */
    public $timestamps = true;


}
