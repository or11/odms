<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DocReceive extends Model
{
    /**
     * The Table
     * @var String
     */
    protected $table = 'doc_receive';

    /**
     * The Primary Key
     * @var integer
     */
    protected $primaryKey = 'drec_id';


    /**
     * Time Stamps
     * @var Bool
     */
    public $timestamps = true;
}
