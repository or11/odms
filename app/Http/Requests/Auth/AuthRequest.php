<?php

namespace App\Http\Requests\Auth;

use Illuminate\Foundation\Http\FormRequest;
// use App\Http\Requests\Request;

class AuthRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(){
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(){
        return [
            'usr_mail' => 'required',
            'usr_password' => 'required'
        ];
    }

    public function messages(){
        return [
            'usr_mail' => 'Your email was wrong',
            'usr_password' => 'Your password was mismatched'
        ];
    }
}
