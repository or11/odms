<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class AdminManager
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next){
        if(Auth::check() && (Auth::user()->isAdmin() || Auth::user()->isManager())){
            return $next($request);
        }
        return redirect('unauthorize');
    }
}
