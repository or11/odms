<?php

namespace App\Http\Controllers\DB;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

/**
 * USE REQUEST CLASS
 */
use App\Http\Requests\InsertUserRequest;
use App\Http\Requests\DocumentRequest;
use App\Http\Requests\DepartRequest;
use App\Http\Requests\PositionRequest;
use App\Http\Requests\ChemicalsRequest;
use App\Http\Requests\Search;
use App\Http\Requests\MSDS;
use App\Http\Requests\Manual;
use App\Http\Requests\Mi;
use App\Http\Requests\Standard;
use App\Http\Requests\returnDoc;
use DB;
use Session;
use Auth;
use PDF;

class DBController extends Controller
{

    /**
     * DOCUMENT FILE PATH.
     */
    protected $manualFilePath = 'assets/file_document/manual';
    protected $miFilePath = 'assets/file_document/mi';
    protected $msdsFilePath = 'assets/file_document/msds';
    protected $standardFilePath = 'assets/file_document/standard';
    
    public function InsertChemicals(ChemicalsRequest $request){
    	$che_code = $request->input('che_code');
    	$che_short_name = $request->input('che_short_name');
    	$che_long_name = $request->input('che_long_name');
    	$che_status = $request->input('che_status');
    	$date = date('Y-m-d H:i:s');
	    $Chemicals = DB::table('_chemicals')->insertGetId([
        	'che_code' => $che_code,
        	'che_short_name' => $che_short_name,
        	'che_long_name' => $che_long_name,
        	'che_status' => $che_status ,
        	'created_at' => $date,
        	'updated_at' => $date
        ]);

        $result = DB::table('_chemicals')->get();
	    return view('dashgum.chemicals', ['result' => $result]);
    }

    public function getChemicals(){
      $result = DB::table('_chemicals')->get();
      return view('dashgum.chemicals', ['result' => $result]);
    }

    public function InsertDepartment(DepartRequest $request){
    	$dep_short_name = $request->input('dep_short_name');
    	$dep_long_name = $request->input('dep_long_name');
    	$dep_email = $request->input('dep_email');
    	$dep_tel = $request->input('dep_tel');
    	$dep_status = $request->input('dep_status');
    	$date = date('Y-m-d H:i:s');
	    $Department = DB::table('_department')->insertGetId([
        	'dep_short_name' => $dep_short_name,
        	'dep_long_name' => $dep_long_name,
        	'dep_email' => $dep_email,
        	'dep_tel' => $dep_tel ,
        	'dep_status' => $dep_status,
        	'created_at' => $date,
        	'updated_at' => $date
        ]);
        $result = DB::table('_department')->get();
	   return view('dashgum.department',['result' => $result]);
    }
    public function getDepartment(){
      $result = DB::table('_department')->get();
      return view('dashgum.department', ['result' => $result]);
    }
    public function InsertDocumentType(DocumentRequest $request){
	    $dt_doc_code = $request->input('dt_doc_code');
    	$dt_short_name = $request->input('dt_short_name');
    	$dt_long_name = $request->input('dt_long_name');
    	$dt_status = $request->input('dt_status');
     	$date = date('Y-m-d H:i:s');
        
	    $DocumentType = DB::table('_document_type')->insertGetId([
        	'dt_doc_code' => $dt_doc_code,
        	'dt_short_name' => $dt_short_name,
        	'dt_long_name' => $dt_long_name,
         	'dt_status' => $dt_status,
        	'created_at' => $date,
        	'updated_at' => $date
        ]);
	    	
	    $result = DB::table('_document_type')->get();

	   return view('dashgum.document',['result' => $result]);
    }
    public function getDocumentType(){
      $result = DB::table('_document_type')->get();
      return view('dashgum.document', ['result' => $result]);
    }
    public function InsertPosition(PositionRequest $request){
    	 $pos_short_name = $request->input('pos_short_name');
    	$pos_long_name = $request->input('pos_long_name');
     	$pos_status = $request->input('pos_status');
     	$date = date('Y-m-d H:i:s');
	    $DocumentType = DB::table('_position')->insertGetId([
        	'pos_short_name' => $pos_short_name,
        	'pos_long_name' => $pos_long_name,
          	'pos_status' => $pos_status,
        	'created_at' => $date,  
        	'updated_at' => $date
        ]);
        $result = DB::table('_position')->get();
	   return view('dashgum.position', ['result' => $result]);
    }

    public function getPosition(){
      $result = DB::table('_position')->get();
      return view('dashgum.position', ['result' => $result]);
    }
   public function Insertborrow(borrow $request){
   		$dbor_user_id = $request->input('dbor_user_id');
   		$dbor_doc_id = $request->input('dbor_doc_id');
   		$dbor_doc_table = $request->input('dbor_doc_table');
 		$date = date('Y-m-d H:i:s');
   		$status = $request->input('status');
   		$Borrow = DB::table('doc_borrow')->insertGetId([
   			'dbor_user_id'=>$dbor_user_id,
   			'dbor_doc_id'=>$dbor_doc_id,
   			'dbor_doc_table'=>$dbor_doc_table,
   			'dbor_borrow_date'=>$date,
   			'dbor_expiry_date'=>$date,
   			'dobr_return_date'=>$date,
   			'status'=>$status,
   			'created_at'=>$date,
   			'updated_at'=>$date]);
   		$result = DB::table('doc_borrow')->get();
   		return view('dashgum.borrow',['result'=>$result]);
   	}
   	public function Insertmsds(MSDS $request){
      /**
       * UPLOADING FILE
       */
      $file = $request->file('drec_file_path');
      if($request->hasFile('drec_file_path') && $file->isValid()){
        $newname = "msds".substr(Session::getId(), 3, 6).rand(1, 9999).time();
        $newname .= ".pdf";
        $path = $file->storeAs($this->msdsFilePath, $newname);
      }else{
        return back()->with('msg', 'การอัพโหลดไฟล์ ผิดพลาด');
      }
      /*============================================================*/

      if($path){
        /**
         * MSDS
         */
        $msds_doc_code = $request->input('msds_doc_code');
        $msds_chem_name = $request->input('msds_chem_name');
        $msds_chem_type = $request->input('msds_chem_type');
        $msds_company_name = $request->input('msds_company_name');
        $msds_doc_date = $request->input('msds_doc_date');
        $date = date('Y-m-d H:i:s');
        $status = $request->input('msds_status');
        $MSDS = DB::table('msds')->insertGetId([
          'msds_doc_code'=>$msds_doc_code,
          'msds_chem_name'=>$msds_chem_name,
          'msds_chem_type'=>$msds_chem_type,
          'msds_company_name'=>$msds_company_name,
          'msds_doc_date'=>$date,
          'status'=>$status,
          'created_at'=>$date,
          'updated_at'=>$date
        ]);
        /*============================================================*/

        /**
         * DOC RECEIVE
         */
    		$drec_doc_table = $request->input('drec_doc_table');
     		$drec_doc_type_id = '';
     		$drec_receive_code = $request->input('drec_receive_code');
     		$date = date('Y-m-d H:i:s');
     		$drec_store_place = $request->input('drec_store_place');
     		$status = $request->input('receive_status');
     		$DocRecevice = DB::table('doc_receive')->insertGetId([
     		 	'drec_doc_id'=>$MSDS,
     		 	'drec_doc_table'=>$drec_doc_table,
     		 	'drec_doc_type_id'=>$drec_doc_type_id,
     		 	'drec_receive_code'=>$drec_receive_code,
     		 	'drec_date'=>$date,
     		 	'drec_store_place'=>$drec_store_place,
     		 	'drec_file_path'=> $path,
     		 	'status'=>$status,
     		 	'created_at'=>$date,
     		 	'updated_at'=>$date
        ]);
        /*========================================================*/
      }
  		return redirect('doc/receive/msds');
    }
    
   	// public function Insertdoc_receive(Receive $request){
   	// 	$drec_doc_id = $request->input('drec_doc_id');
   	// 	$drec_doc_table = $request->input('drec_doc_table');
   	// 	$drec_doc_type_id = $request->input('drec_doc_type_id');
   	// 	$drec_receive_code = $request->input('drec_receive_code');
   	// 	$date = date('Y-m-d H:i:s');
   	// 	$drec_store_place = $request->input('drec_store_place');
   	// 	$drec_file_path = $request->input('drec_file_path');
   	// 	$status = $request->input('status');
   	// 	 $DocRecevice = DB::table('doc_receive')->insertGetId([
   	// 	 	'drec_doc_id'=>$drec_doc_id,
   	// 	 	'drec_doc_table'=>$drec_doc_table,
   	// 	 	'drec_doc_type_id'=>$drec_doc_type_id,
   	// 	 	'drec_receive_code'=>$drec_receive_code,
   	// 	 	'drec_date'=>$date,
   	// 	 	'drec_store_place'=>$drec_store_place,
   	// 	 	'drec_file_path'=>$drec_file_path,
   	// 	 	'status'=>$status,
   	// 	 	'created_at'=>$date,
   	// 	 	'updated_at'=>$date]);
   	// 	 $result = DB::table('doc_receive')->get();
   		
   	// 	 return view('dashgum.msds',['result'=>$result]);
   	// }
   	public function InsertManual(Manual $request){

      $file = $request->file('drec_file_path');
      if($request->hasFile('drec_file_path') && $file->isValid()){
        $newname = "manual".substr(Session::getId(), 3, 6).rand(1, 9999).time();
        $newname .= ".pdf";
        $path = $file->storeAs($this->manualFilePath, $newname);
      }else{
        return back()->with('msg', 'การอัพโหลดไฟล์ผิดพลาด');
      }

      if($path){
       	$manual_doc_code =   $request->input('manual_doc_code');
       	$manual_machine_name =  $request->input('manual_machine_name');
       	$manual_department_id =  $request->input('manual_department_id');
        $date = date('Y-m-d H:i:s');
        $status = $request->input('manual_status');
        $Manual = DB::table('manual')->insertGetId([
          'manual_doc_code'=>$manual_doc_code,
          'manual_machine_name'=>$manual_machine_name,
          'manual_department_id'=>$manual_department_id,
          'manual_doc_date'=>$date,
          'status'=>$status,
          'created_at'=>$date,
          'updated_at'=>$date
        ]);

      	$drec_doc_table = $request->input('drec_doc_table');
     		$drec_doc_type_id = '';
     		$drec_receive_code = $request->input('drec_receive_code');
     		$date = date('Y-m-d H:i:s');
     		$drec_store_place = $request->input('drec_store_place');
     		$status_receive = $request->input('receive_status');
   		  $DocRecevice = DB::table('doc_receive')->insertGetId([
     		 	'drec_doc_id'=>$Manual,
     		 	'drec_doc_table'=>$drec_doc_table,
     		 	'drec_doc_type_id'=>$drec_doc_type_id,
     		 	'drec_receive_code'=>$drec_receive_code,
     		 	'drec_date'=>$date,
     		 	'drec_store_place'=>$drec_store_place,
     		 	'drec_file_path'=>$path,
     		 	'status'=>$status_receive,
     		 	'created_at'=>$date, 
     		 	'updated_at'=>$date
        ]);
      }

 		 	return redirect('doc/receive/manual');
   	}
   	public function InsertMi(Mi $request){
      $file = $request->file('drec_file_path');
      if($request->hasFile('drec_file_path') && $file->isValid()){
        $newname = "mi".substr(Session::getId(), 3, 6).rand(1, 9999).time();
        $newname .= ".pdf";
        $path = $file->storeAs($this->miFilePath, $newname);
      }else{
        return back()->with('msg', 'การอัพโหลดไฟล์ผิดพลาด');
      }
      if($path){
     		$mi_doc_code = $request->input('mi_doc_code');
     		$mi_doc_no = $request->input('mi_doc_no');
     		$mi_doc_name = $request->input('mi_doc_name');
     		$mi_change_no = 0;
     		$mi_description = $request->input('mi_description');
     		$mi_designer = $request->input('mi_designer');
     		$date = date('Y-m-d H:i:s');
     		$status = $request->input('mi_status');
     		$MI = DB::table('mi')->insertGetId([
     			'mi_doc_code'=>$mi_doc_code,
     			'mi_doc_no'=>$mi_doc_no,
     			'mi_doc_name'=>$mi_doc_name,
     			'mi_change_no'=>$mi_change_no,
     			'mi_description'=>$mi_description,
     			'mi_designer'=>$mi_designer,
     			'mi_doc_date'=>$date,
     			'status'=>$status,
     			'created_at'=>$date,
     			'updated_at'=>$date
        ]);

  		$drec_doc_table = $request->input('drec_doc_table');
     		$drec_doc_type_id = '';
     		$drec_receive_code = $request->input('drec_receive_code');
     		$date = date('Y-m-d H:i:s');
     		$drec_store_place = $request->input('drec_store_place');
     		$status_receive = $request->input('receive_status');
     		$DocRecevice = DB::table('doc_receive')->insertGetId([
     		 	'drec_doc_id'=>$MI,
     		 	'drec_doc_table'=>$drec_doc_table,
     		 	'drec_doc_type_id'=>$drec_doc_type_id,
     		 	'drec_receive_code'=>$drec_receive_code,
     		 	'drec_date'=>$date,
     		 	'drec_store_place'=>$drec_store_place,
     		 	'drec_file_path'=>$path,
     		 	'status'=>$status_receive,
     		 	'created_at'=>$date, 
     		 	'updated_at'=>$date
        ]);
      }
 		 	return redirect('doc/receive/mi');

   	}

   	public function Insertstandard(Standard $request){
      $file = $request->file('drec_file_path');
      if($request->hasFile('drec_file_path') && $file->isValid()){
        $newname = "standard".substr(Session::getId(), 3, 6).rand(1, 9999).time();
        $newname .= ".pdf";
        $path = $file->storeAs($this->standardFilePath, $newname);
      }else{
        return back()->with('msg', 'การอัพโหลดไฟล์ผิดพลาด');
      }

      if($path){
     		$standard_doc_code = $request->input('standard_doc_code');
     		$standard_doc_name = $request->input('standard_doc_name');
     		$standard_description = $request->input('standard_description');
     		$standard_doc_type = $request->input('standard_doc_type');
      	$date = date('Y-m-d H:i:s');
     		$status = $request->input('standard_status');
     		$Standard = DB::table('standard')->insertGetId([
     			'standard_doc_code'=>$standard_doc_code,
     			'standard_doc_name'=>$standard_doc_name,
     			'standard_description'=>$standard_description,
     			'standard_doc_type'=>$standard_doc_type,
     			'standard_doc_date'=>$date,
     			'status'=>$status,
     			'created_at'=>$date,
     			'updated_at'=>$date
        ]);
     		$drec_doc_table = $request->input('drec_doc_table');
     		$drec_doc_type_id = '';
     		$drec_receive_code = $request->input('drec_receive_code');
     		$date = date('Y-m-d H:i:s');
     		$drec_store_place = $request->input('drec_store_place');
     		$status_receive = $request->input('receive_status');
     		 $DocRecevice = DB::table('doc_receive')->insertGetId([
     		 	'drec_doc_id'=>$Standard,
     		 	'drec_doc_table'=>$drec_doc_table,
     		 	'drec_doc_type_id'=>$drec_doc_type_id,
     		 	'drec_receive_code'=>$drec_receive_code,
     		 	'drec_date'=>$date,
     		 	'drec_store_place'=>$drec_store_place,
     		 	'drec_file_path'=>$path,
     		 	'status'=>$status_receive,
     		 	'created_at'=>$date, 
     		 	'updated_at'=>$date
        ]);
      }
 		 	return redirect('doc/receive/standard');
   	}


    // public function InsertPrefixName(InsertUserRequest $request){
    //     $pfn_short_name = $request->input('pfn_short_name');
    // 	$pfn_long_name = $request->input('pfn_long_name');
    //  	$pfn_status = $request->input('pfn_status');
    //  	$date = date('Y-m-d H:i:s');
		   //  $DocumentType = DB::table('_prefix_name')->insertGetId([
    //     	'pfn_short_name' => 'short_name',
    //     	'pfn_long_name' => 'long_name',
    //       	'pfn_status' => $pfn_status,
    //     	'created_at' => $date,  
    //     	'updated_at' => $date
    //     ]);
    // 	return view('welcome');
    // }
    // public function InsertUserRole(InsertUserRequest $request){
 	  //   $ur_short_name = $request->input('ur_short_name');
    // 	$ur_long_name = $request->input('ur_long_name');
    //  	$ur_status = $request->input('ur_status');
    //  	$date = date('Y-m-d H:i:s');
	   //  $DocumentType = DB::table('_user_role')->insertGetId([
    //     	'ur_short_name' => $ur_short_name,
    //     	'ur_long_name' => $ur_long_name,
    //       	'ur_status' => $ur_status,
    //     	'created_at' => $date,  
    //     	'updated_at' => $date
    //     ]);
    // 	return view('welcome');
    // }


    public function UpdateChemicals($id = '',Request $request){
          $che_short_name = $request->input('che_short_name');
          $che_long_name  = $request->input('che_long_name');
          $che_status = $request->input('che_status');
    		$ChemicalsUpdate = DB::table('_chemicals')
            ->where('che_id', $id)	
            ->update(['che_short_name' => $che_short_name,
            	'che_long_name'=>$che_long_name,
            	'che_status'=>$che_status]);
                  $result = DB::table('_chemicals')->get();
	 		  return view('dashgum.chemicals',['result' => $result]);
    }
    public function UpdateDepartment($id = '',Request $request){
    	$dep_short_name = $request->input('dep_short_name');
    	$dep_long_name = $request->input('dep_long_name');
    	$dep_email = $request->input('dep_email');
    	$dep_tel = $request->input('dep_tel');
    	$dep_status = $request->input('dep_status');
    	$DepartmentUpdate = DB::table('_department')->where('dep_id',$id)->update([
    		'dep_short_name'=>$dep_short_name,
    		'dep_long_name'=>$dep_long_name,
    		'dep_email'=>$dep_email,
    		'dep_tel'=>$dep_tel,
    		'dep_status'=>$dep_status]);
    	$result = DB::table('_department')->get();
    	return view('dashgum.department',['result'=>$result]);
    	    }
    public function UpdateDocumentType($id = '',Request $request){
    	//
        $dt_doc_code = $request->input('dt_doc_code');
    	$dt_short_name = $request->input('dt_short_name');
    	$dt_long_name = $request->input('dt_long_name');
    	$dt_status = $request->input('dt_status');
    	$DocumentTypeUpdate = DB::table('_document_type')->where('dt_id',$id)->update([
    		'dt_doc_code' => $dt_doc_code,
    		'dt_short_name' => $dt_short_name,
    		'dt_long_name' => $dt_long_name,
    		'dt_status' => $dt_status]);
    	$result = DB::table('_document_type')->get();
    	return view('dashgum.document',['result'=>$result]);
    }
    public function UpdatePosition($id = '',Request $request){
    	//
        $pos_short_name = $request->input('pos_short_name');
    	$pos_long_name = $request->input('pos_long_name');
     	$pos_status = $request->input('pos_status');
     	$PositionUpdate = DB::table('_position')->where('pos_id',$id)->update([
     		'pos_short_name'=> $pos_short_name,
     		'pos_long_name'=>$pos_long_name,
     		'pos_status'=>$pos_status]);
     	$result = DB::table('_position')->get();
     	return view('dashgum.position',['result'=>$result]);
    }
    // public function UpdatePrefixName($id = 
    // 	//
    // }
    // public function UpdateUserRole($id = ''){
    // 	//
    // }	
    public function Deletechemicals($id=''){
    	$ChemicalsDelete = DB::table('_chemicals')->where('che_id',$id)->delete();
    	$result = DB::table('_chemicals')->get();
    	return view('dashgum.chemicals',['result'=>$result]);
    }
    public function DeleteDepartment($id=''){
    	$DepartmentDelete = DB::table('_department')->where('dep_id',$id)->delete();
    	$result = DB::table('_department')->get();
    	return view('dashgum.department',['result'=>$result]);
    }
    public function DeleteDocument($id=''){
    	$DocumentDelete = DB::table('_document_type')->where('dt_id',$id)->delete();
    	$result = DB::table('_document_type')->get();
    	return view('dashgum.document',['result'=>$result]);
    }
    public function DeletePosition($id=''){
    	$Position = DB::table('_position')->where('pos_id',$id)->delete();
    	$result = DB::table('_position')->get();
    	return view('dashgum.position',['result'=>$result]);
    }

    public function search(Search $request){
	    $dt_doc_code = $request->input('dt_doc_code');
      $result = array();
      switch ($request->input('doc_type')) {
        case 'msds':
          $result['msds'] = $this->findMSDSById($dt_doc_code);
          break;
        case 'mi':
          $result['mi'] = $this->findMIById($dt_doc_code);
          break;
        case 'standard':
          $result['standard'] = $this->findStandardById($dt_doc_code);
          break;
        case 'manual':
          $result['manual'] = $this->findManualById($dt_doc_code);
          break;
        default:
          $result = [
            'msds' => $this->findMSDSById($dt_doc_code),
            'mi' => $this->findMIById($dt_doc_code),
            'standard' => $this->findStandardById($dt_doc_code),
            'manual' => $this->findManualById($dt_doc_code)
          ];
      } 
   	   return view('dashgum.search',$result);
    }

    public function findManualById($id = ''){
      return DB::table('manual')
                ->join('_department', 'dep_id', 'manual_department_id')
                ->join('doc_receive', function($join){
                    $join->on('drec_doc_id', 'manual_id')
                      ->where('drec_doc_table', 'manual');
                })
                ->where('manual_doc_code', $id)
                ->orWhere('manual_machine_name','like', "%$id%")
                ->orWhere('dep_short_name','like', "%$id%")
                ->orWhere('dep_long_name','like', "%$id%")
                ->get();
    }

    public function findStandardById($id = ''){
      return DB::table('standard')
                ->join('_document_type', 'dt_id', 'standard_doc_type')
                ->join('doc_receive', function($join){
                    $join->on('drec_doc_id', 'standard_id')
                      ->where('drec_doc_table', 'standard');
                })
                ->where('standard_doc_code', $id)
                ->orWhere('standard_doc_name','like', "%$id%")
                ->orWhere('standard_description','like', "%$id%")
                ->orWhere('dt_short_name','like', "%$id%")
                ->orWhere('dt_long_name','like', "%$id%")
                ->get();
    }

    public function findMIById($id = ''){
      return DB::table('mi')
              ->join('doc_receive', function($join){
                  $join->on('drec_doc_id', 'mi_id') 
                    ->where('drec_doc_table', 'mi');
              })
              ->where('mi_doc_code', $id)
              ->orWhere('mi_doc_no', $id)
              ->orWhere('mi_doc_name', 'like', "%$id%")
              ->orWhere('mi_description', 'like', "%$id%")
              ->orWhere('mi_designer', 'like', "%$id%")
              ->get();
    }

    public function findMSDSById($id =''){
      return DB::table('msds')
              ->join('_chemicals', 'che_id', '=', 'msds_chem_type')
              ->join('doc_receive', function($join){
                  $join->on('drec_doc_id', '=', 'msds_id')
                    ->where('drec_doc_table', '=', 'msds');
              })
              ->where('msds_doc_code', $id)
              ->orWhere('msds_chem_name', 'like', "%$id%")
              ->orWhere('msds_company_name', 'like', "%$id%")
              ->orWhere('che_short_name', 'like', "%$id%")
              ->orWhere('che_long_name', 'like', "%$id%")
              ->get();
    }

    public function detailDocumentByAdmin($type, $id){
      /**
       * FIND DOC DETAIL.
       */
      switch ($type) {
        case 'msds':
          $result['msds'] = $this->findMSDSById($id);
          break;
        case 'mi':
          $result['mi'] = $this->findMIById($id);
          break;
        case 'standard':
          $result['standard'] = $this->findStandardById($id);
          break;
        case 'manual':
          $result['manual'] = $this->findManualById($id);
          break;
      }

      /**
       * FETCH BORROW STATUS
       */
      $result['bstatus'] = (sizeof($result[$type]) > 0 && ($result[$type][0]->status == 'available')) ? true : false;
      return view('dashgum.detail', $result);
      // return var_dump($result[$type]);
    }

    public function detailDocumentByUser($type, $id){
      /**
       * FIND DOC DETAIL.
       */
      switch ($type) {
        case 'msds':
          $result['msds'] = $this->findMSDSById($id);
          break;
        case 'mi':
          $result['mi'] = $this->findMIById($id);
          break;
        case 'standard':
          $result['standard'] = $this->findStandardById($id);
          break;
        case 'manual':
          $result['manual'] = $this->findManualById($id);
          break;
      }

      /**
       * FETCH BORROW STATUS
       */
      $result['bstatus'] = (sizeof($result[$type]) > 0 && ($result[$type][0]->status == 'available')) ? true : false;
      return view('dashgum.view', $result);
      // return var_dump($result[$type]);
    }

    public function getRequestStatus(){
      $user_id = Auth::user()->usr_id;

      $SQL = "SELECT
                doc_borrow.dbor_id,
                doc_borrow.dbor_user_id,
                doc_borrow.dbor_doc_id,
                doc_borrow.dbor_doc_table,
                doc_borrow.dbor_borrow_date,
                doc_borrow.dbor_expiry_date,
                doc_borrow.dbor_return_date,
                doc_borrow.`status`,
                doc_borrow.created_at,
                doc_borrow.updated_at,
                mi.mi_id,
                mi.mi_doc_code,
                mi.mi_doc_no,
                mi.mi_doc_name,
                mi.mi_designer,
                msds.msds_id,
                msds.msds_doc_code,
                msds.msds_chem_name,
                msds.msds_company_name,
                standard.standard_id,
                standard.standard_doc_code,
                standard.standard_doc_name,
                manual.manual_id,
                manual.manual_doc_code,
                manual.manual_machine_name,
                manual.manual_doc_date
                FROM doc_borrow
                LEFT JOIN mi ON doc_borrow.dbor_doc_id = mi.mi_id 
                  AND doc_borrow.dbor_doc_table = 'mi'
                LEFT JOIN msds ON doc_borrow.dbor_doc_id = msds.msds_id 
                  AND doc_borrow.dbor_doc_table = 'msds'
                LEFT JOIN standard ON doc_borrow.dbor_doc_id = standard.standard_id 
                  AND doc_borrow.dbor_doc_table = 'standard'
                LEFT JOIN manual ON doc_borrow.dbor_doc_id = manual.manual_id 
                  AND doc_borrow.dbor_doc_table = 'manual'
                WHERE doc_borrow.dbor_user_id = '$user_id' ";
      $result['result']['borrow'] = DB::select($SQL);

      $SQL = "SELECT
                doc_borrow_file.dbor_id,
                doc_borrow_file.dbor_user_id,
                doc_borrow_file.dbor_doc_id,
                doc_borrow_file.dbor_doc_table,
                doc_borrow_file.dbor_borrow_date,
                doc_borrow_file.dbor_expiry_date,
                doc_borrow_file.dbor_return_date,
                doc_borrow_file.`status`,
                doc_borrow_file.created_at,
                doc_borrow_file.updated_at,
                mi.mi_id,
                mi.mi_doc_code,
                mi.mi_doc_no,
                mi.mi_doc_name,
                mi.mi_designer,
                msds.msds_id,
                msds.msds_doc_code,
                msds.msds_chem_name,
                msds.msds_company_name,
                standard.standard_id,
                standard.standard_doc_code,
                standard.standard_doc_name,
                manual.manual_id,
                manual.manual_doc_code,
                manual.manual_machine_name,
                manual.manual_doc_date,
                doc_receive.drec_doc_id,
                doc_receive.drec_file_path
                FROM doc_borrow_file
                LEFT JOIN doc_receive ON doc_borrow_file.dbor_doc_id = doc_receive.drec_doc_id 
                  AND doc_borrow_file.dbor_doc_table = doc_receive.drec_doc_table
                LEFT JOIN mi ON doc_borrow_file.dbor_doc_id = mi.mi_id 
                  AND doc_borrow_file.dbor_doc_table = 'mi'
                LEFT JOIN msds ON doc_borrow_file.dbor_doc_id = msds.msds_id 
                  AND doc_borrow_file.dbor_doc_table = 'msds'
                LEFT JOIN standard ON doc_borrow_file.dbor_doc_id = standard.standard_id 
                  AND doc_borrow_file.dbor_doc_table = 'standard'
                LEFT JOIN manual ON doc_borrow_file.dbor_doc_id = manual.manual_id 
                  AND doc_borrow_file.dbor_doc_table = 'manual'
                WHERE doc_borrow_file.dbor_user_id = '$user_id' ";
      $result['result']['file'] = DB::select($SQL);

      return view('dashgum.request_status', $result);
    }

    public function makeFileBorrow($type, $id){
      $id = base64_decode($id);

      switch ($type) {
        case 'msds':
          $result = $this->findMSDSById($id);
          $doc_id = $result[0]->msds_id;
          break;
        case 'mi':
          $result = $this->findMIById($id);
          $doc_id = $result[0]->mi_id;
          break;
        case 'standard':
          $result = $this->findStandardById($id);
          $doc_id = $result[0]->standard_id;
          break;
        case 'manual':
          $result = $this->findManualById($id);
          $doc_id = $result[0]->manual_id;
          break;
      }
/*
      DB::table('doc_receive')->where([
          ['drec_doc_id', '=', $doc_id],
          ['drec_doc_table', '=', $type]
        ])
        ->update(['status' => 'pending']);*/

      $date = date("Y-m-d");
      $datetime = date("Y-m-d H:i:s");
      DB::table('doc_borrow_file')->insert([
        [
          'dbor_user_id' => Auth::user()->usr_id,
          'dbor_doc_id' => $doc_id,
          'dbor_doc_table' => $type,
          'dbor_borrow_date' => $date,
          'dbor_expiry_date' => $this->plusDate($date),
          'dbor_return_date' => $date,
          'status' => 'pending',
          'created_at' => $datetime,
          'updated_at' => $datetime
        ]
      ]);

      return redirect('doc/search');
    }

    public function makeBorrow($type, $id){
      switch ($type) {
        case 'msds':
          $result = $this->findMSDSById($id);
          $doc_id = $result[0]->msds_id;
          break;
        case 'mi':
          $result = $this->findMIById($id);
          $doc_id = $result[0]->mi_id;
          break;
        case 'standard':
          $result = $this->findStandardById($id);
          $doc_id = $result[0]->standard_id;
          break;
        case 'manual':
          $result = $this->findManualById($id);
          $doc_id = $result[0]->manual_id;
          break;
      }

      DB::table('doc_receive')->where([
          ['drec_doc_id', '=', $doc_id],
          ['drec_doc_table', '=', $type]
        ])
        ->update(['status' => 'pending']);

      $date = date("Y-m-d");
      $datetime = date("Y-m-d H:i:s");
      DB::table('doc_borrow')->insert([
        [
          'dbor_user_id' => Auth::user()->usr_id,
          'dbor_doc_id' => $doc_id,
          'dbor_doc_table' => $type,
          'dbor_borrow_date' => $date,
          'dbor_expiry_date' => $this->plusDate($date),
          'dbor_return_date' => $date,
          'status' => 'pending',
          'created_at' => $datetime,
          'updated_at' => $datetime
        ]
      ]);

      return redirect('doc/search');
    }

    public function getMSDS(){
      $chm = DB::table('_chemicals')->orderBy('che_id')->get();
      return view('dashgum.msds', ['chems' => $chm]);
    }

    public function getManual(){
      $deps = DB::table('_department')->orderBy('dep_id')->get();
      return view('dashgum.manual', ['dep' => $deps]);
    }

    public function getStandard(){
      $doc_type = DB::table('_document_type')->orderBy('dt_id')->get();
      return view('dashgum.standard', ['doc_type' => $doc_type]);
    }

    public function returnDoc(Request $request){
        $drec_doc_id = $request->input('dbor_doc_id');
        $return_status = $request->input('return_status');
     	$PositionUpdate = DB::table('doc_borrow')->where('dbor_doc_id',$drec_doc_id)->update([
     	 	'status'=>$return_status]);
     	// $result = DB::table('doc_borrow')->get();
     	$result = "คืนสำเร็จ";
     	return view('dashgum.returndoc',[$result]);
    }

    public function returningDoc($type, $id){

        DB::table('doc_receive')
            ->where([
                ['drec_doc_table', $type],
                ['drec_doc_id', $id]
            ])->update(['status' => 'available']);

        DB::table('doc_borrow')
            ->where([
                ['dbor_doc_table', $type],
                ['dbor_doc_id', $id]
            ])->update(['status' => 'return', 'dbor_return_date' => date("Y-m-d")]);

        return redirect('doc/search');
    }
    
    public function viewBorrowList(){
      $SQL = "SELECT
            doc_receive.drec_id,
            doc_receive.drec_doc_id,
            doc_receive.drec_doc_table,
            doc_receive.drec_doc_type_id,
            doc_receive.drec_receive_code,
            doc_receive.drec_date,
            doc_receive.drec_store_place,
            doc_receive.drec_file_path,
            doc_receive.`status`,
            doc_receive.created_at,
            doc_receive.updated_at,
            doc_borrow.dbor_id,
            doc_borrow.dbor_user_id,
            doc_borrow.dbor_doc_id,
            doc_borrow.dbor_borrow_date,
            doc_borrow.dbor_expiry_date,
            doc_borrow.dbor_return_date,
            doc_borrow.`status`,
            doc_borrow.created_at,
            doc_borrow.updated_at,
            doc_borrow.dbor_doc_table,
            users.usr_id,
            users.usr_department,
            users.usr_code,
            users.usr_name,
            users.usr_lastname,
            users.email,
            _department.dep_long_name,
            _department.dep_short_name,
            _department.dep_email
            FROM doc_receive
            INNER JOIN doc_borrow 
                ON doc_receive.drec_doc_id = doc_borrow.dbor_doc_id 
                  AND doc_receive.drec_doc_table = doc_borrow.dbor_doc_table
            INNER JOIN users ON doc_borrow.dbor_user_id = users.usr_id
            INNER JOIN _department ON users.usr_department = _department.dep_id
            WHERE
            doc_receive.`status` = 'pending' AND
            doc_borrow.`status` = 'pending'
            ORDER BY
            doc_borrow.created_at DESC ";
      $result = DB::select($SQL);

      /**
       * FILE 
       */
      $SQL = "SELECT
            doc_receive.drec_id,
            doc_receive.drec_doc_id,
            doc_receive.drec_doc_table,
            doc_receive.drec_doc_type_id,
            doc_receive.drec_receive_code,
            doc_receive.drec_date,
            doc_receive.drec_store_place,
            doc_receive.drec_file_path,
            doc_receive.`status`,
            doc_receive.created_at,
            doc_receive.updated_at,
            doc_borrow_file.dbor_id,
            doc_borrow_file.dbor_user_id,
            doc_borrow_file.dbor_doc_id,
            doc_borrow_file.dbor_borrow_date,
            doc_borrow_file.dbor_expiry_date,
            doc_borrow_file.dbor_return_date,
            doc_borrow_file.`status`,
            doc_borrow_file.created_at,
            doc_borrow_file.updated_at,
            doc_borrow_file.dbor_doc_table,
            users.usr_id,
            users.usr_department,
            users.usr_code,
            users.usr_name,
            users.usr_lastname,
            users.email,
            _department.dep_long_name,
            _department.dep_short_name,
            _department.dep_email
            FROM doc_receive
            INNER JOIN doc_borrow_file 
                ON doc_receive.drec_doc_id = doc_borrow_file.dbor_doc_id 
                  AND doc_receive.drec_doc_table = doc_borrow_file.dbor_doc_table
            INNER JOIN users ON doc_borrow_file.dbor_user_id = users.usr_id
            INNER JOIN _department ON users.usr_department = _department.dep_id
            WHERE doc_borrow_file.`status` = 'pending'
            ORDER BY doc_borrow_file.created_at DESC ";

      $file = DB::select($SQL);
      // return var_dump($result);
      return view('dashgum.borrow', ['result' => $result, 'file' => $file]);
    }

    public function makeFileApprove($type, $receive){
      $rec_status = $type == 'pass' ? 'borrow' : 'available';
      $bor_status = $type == 'pass' ? 'borrow' : 'cancelled';
      $receive = base64_decode($receive);
      /*$result = DB::table('doc_receive')
                ->join('doc_borrow', function($join){
                    $join->on('dbor_doc_id', '=', 'drec_doc_id')
                    ->where('dbor_doc_table', 'drec_doc_table');
                })
                ->where('drec_receive_code', $receive)->get();*/
      $SQL = "SELECT * FROM `doc_receive`
                INNER JOIN `doc_borrow_file` ON `dbor_doc_id` = `drec_doc_id`
                AND (
                  `dbor_doc_table` = drec_doc_table
                )
                WHERE `drec_receive_code` = '$receive' ";
      $result = DB::select($SQL);
      // return var_dump($result);
      if(sizeof($result)>0){
        /**
         * UPDATE
         */
        DB::table('doc_borrow_file')->where('dbor_id', $result[0]->dbor_id)
            ->update(['status' => $bor_status]);
      }else{
        return 'not found';
      }
      return redirect('doc/borrow');
    }

    public function makeApprove($type, $receive){
      $rec_status = $type == 'pass' ? 'borrow' : 'available';
      $bor_status = $type == 'pass' ? 'borrow' : 'cancelled';
      $receive = base64_decode($receive);
      /*$result = DB::table('doc_receive')
                ->join('doc_borrow', function($join){
                    $join->on('dbor_doc_id', '=', 'drec_doc_id')
                    ->where('dbor_doc_table', 'drec_doc_table');
                })
                ->where('drec_receive_code', $receive)->get();*/
      $SQL = "SELECT * FROM `doc_receive`
                INNER JOIN `doc_borrow` ON `dbor_doc_id` = `drec_doc_id`
                AND (
                  `dbor_doc_table` = drec_doc_table
                )
                WHERE `drec_receive_code` = '$receive' ";
      $result = DB::select($SQL);
      // return var_dump($result);
      if(sizeof($result)>0){
        /**
         * UPDATE
         */
        DB::table('doc_receive')->where('drec_id', $result[0]->drec_id)
            ->update(['status' => $rec_status]);

        DB::table('doc_borrow')->where('dbor_id', $result[0]->dbor_id)
            ->update(['status' => $bor_status]);
      }else{
        return 'not found';
      }
      return redirect('doc/borrow');
    }

    public function getYearBorrowReport(){
      return view('dashgum.rpt_year_borrow');
    }

    public function postYearBorrowReport(Request $request){
      $SQL = "SELECT
                doc_borrow.dbor_id,
                doc_borrow.dbor_user_id,
                doc_borrow.dbor_doc_id,
                doc_borrow.dbor_doc_table,
                doc_borrow.dbor_borrow_date,
                doc_borrow.dbor_expiry_date,
                doc_borrow.dbor_return_date,
                doc_borrow.`status`,
                doc_borrow.created_at,
                doc_borrow.updated_at,
                users.usr_code,
                users.usr_name,
                users.usr_lastname,
                users.email,
                manual.manual_id,
                manual.manual_doc_code,
                manual.manual_machine_name,
                manual.manual_doc_date,
                manual.created_at,
                manual.updated_at,
                mi.mi_id,
                mi.mi_doc_code,
                mi.mi_doc_no,
                mi.mi_doc_name,
                mi.mi_change_no,
                mi.mi_description,
                mi.mi_designer,
                mi.mi_doc_date,
                mi.created_at,
                mi.updated_at,
                msds.msds_id,
                msds.msds_doc_code,
                msds.msds_chem_name,
                msds.msds_chem_type,
                msds.msds_company_name,
                msds.msds_doc_date,
                msds.created_at,
                msds.updated_at,
                standard.standard_id,
                standard.standard_doc_code,
                standard.standard_doc_name,
                standard.standard_description,
                standard.standard_doc_type,
                standard.standard_doc_date,
                standard.created_at,
                standard.updated_at,
                manual.manual_department_id
                FROM doc_borrow
                LEFT JOIN users ON doc_borrow.dbor_user_id = users.usr_id
                LEFT JOIN manual ON doc_borrow.dbor_doc_id = manual.manual_id 
                  AND doc_borrow.dbor_doc_table = 'manual'
                LEFT JOIN mi ON doc_borrow.dbor_doc_id = mi.mi_id 
                  AND doc_borrow.dbor_doc_table = 'mi'
                LEFT JOIN msds ON doc_borrow.dbor_doc_id = msds.msds_id 
                  AND doc_borrow.dbor_doc_table = 'msds'
                LEFT JOIN standard ON doc_borrow.dbor_doc_id = standard.standard_id 
                  AND doc_borrow.dbor_doc_table = 'standard' ";

      switch ($request->input('b_status')) {
        case 'approve' :
          $SQL .= " WHERE doc_borrow.`status`  IN ('borrow', 'return') ";
          break;
        case 'cancelled' :
          $SQL .= " WHERE doc_borrow.`status` = 'cancelled' ";
          break;
        case 'borrow' :
          $SQL .= " WHERE doc_borrow.`status` = 'borrow' ";
          break;
        case 'return' :
          $SQL .= " WHERE doc_borrow.`status` = 'return' ";
          break;
      }

      $SQL .= " ORDER BY doc_borrow.created_at ASC  ";
      $r['result'] = DB::select($SQL);
      return PDF::loadView('year_borrow',$r)->stream();
    }

    public function getYearReceiveDocReport(){
      $SQL = "SELECT
                doc_receive.drec_id,
                doc_receive.drec_doc_id,
                doc_receive.drec_doc_table,
                doc_receive.drec_doc_type_id,
                doc_receive.drec_receive_code,
                doc_receive.drec_date,
                doc_receive.drec_store_place,
                doc_receive.drec_file_path,
                doc_receive.`status`,
                doc_receive.created_at,
                doc_receive.updated_at,
                manual.manual_id,
                manual.manual_doc_code,
                manual.manual_machine_name,
                manual.manual_department_id,
                manual.manual_doc_date,
                manual.`status`,
                manual.created_at,
                manual.updated_at,
                mi.mi_id,
                mi.mi_doc_code,
                mi.mi_doc_no,
                mi.mi_doc_name,
                mi.mi_change_no,
                mi.mi_description,
                mi.mi_designer,
                mi.mi_doc_date,
                mi.`status`,
                mi.created_at,
                mi.updated_at,
                msds.msds_id,
                msds.msds_doc_code,
                msds.msds_chem_name,
                msds.msds_chem_type,
                msds.msds_company_name,
                msds.msds_doc_date,
                msds.`status`,
                msds.created_at,
                msds.updated_at,
                standard.standard_id,
                standard.standard_doc_code,
                standard.standard_doc_name,
                standard.standard_description,
                standard.standard_doc_type,
                standard.standard_doc_date,
                standard.`status`,
                standard.created_at,
                standard.updated_at
                FROM doc_receive
                LEFT JOIN manual ON doc_receive.drec_doc_id = manual.manual_id 
                  AND doc_receive.drec_doc_table = 'manual'
                LEFT JOIN mi ON doc_receive.drec_doc_id = mi.mi_id 
                  AND doc_receive.drec_doc_table = 'mi'
                LEFT JOIN msds ON doc_receive.drec_doc_id = msds.msds_id 
                  AND doc_receive.drec_doc_table = 'msds'
                LEFT JOIN standard ON doc_receive.drec_doc_id = standard.standard_id 
                  AND doc_receive.drec_doc_table = 'standard' 
                ORDER BY doc_receive.created_at ASC ";

      $receive = DB::select($SQL);
      foreach($receive as $rec){
        switch($rec->drec_doc_table){
          case 'manual' :
            $doc_code = $rec->manual_doc_code;
            $doc_name = $rec->manual_machine_name;
            break;
          case 'mi' :
            $doc_code = $rec->mi_doc_code;
            $doc_name = $rec->mi_doc_name;
            break;
          case 'msds' :
            $doc_code = $rec->msds_doc_code;
            $doc_name = $rec->msds_chem_name;
            break;
          case 'standard' :
            $doc_code = $rec->standard_doc_code;
            $doc_name = $rec->standard_doc_name;
            break;
          default :
            $doc_code = $doc_name = "N/A";
        }
        $data['data'][] = [
          'rec_code' => $rec->drec_receive_code,
          'type' => $rec->drec_doc_table,
          'doc_code' => $doc_code,
          'doc_name' => $doc_name,
          'store' => $rec->drec_store_place,
          'date' => $rec->drec_date
        ];
      }
      // print_r($data);
      return  PDF::loadView('year_receive', $data)->stream();
    }

    /*UTILITY FUNCTION*/
    protected function plusDate($date){
      return date('Y-m-d H:i:s', strtotime("$date +7 days"));
    }

}

