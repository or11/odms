<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

/**
 * EXTENDS USE
 */
use Auth;
use App\Http\Requests\Auth\AuthRequest;
use App\User;
use Hash;

class AuthController extends Controller
{	
	public function getIndex(){
		return 'hey';
        if(!Auth::guest()){
            return "hey";
        }else{
            return redirect()->to('admin/auth');
        }
    }

    /**
     * @func postLogin | Authentication Manager
     * @param LoginRequest $request | Request manager
     * @var String $email | Input email.
     * @var String $pwd | Input password.
     * @return String | Return notice to user.
     * ---------------------------------------------------
     * TODO
     * 1. Use Auth::check() for cheking login status.
     * 2. If (true) then redirect to home page. 
     * 3. If (false) then do login.
     */
    public function postLogin(AuthRequest $request){
        if(!Auth::guest()){
            return redirect('dashboard');
        }else{
	        $email = $request->input('usr_mail');
            $pwd = $request->input('usr_password');
            if(Auth::attempt(["email" => "$email", "password" => "$pwd"])){
                return redirect('dashboard');
            }else{
                return back()->with('Your email or password is mismatch.');
            }
        }
    }

    public function getLogout(){
    	// if(Auth::logout()){
    	// }
        Auth::logout();
        return redirect('auth/login');
    }

    public function show(){
        if(!Auth::guest()){
            $fn = Auth::user()->usr_name;
            $ln = Auth::user()->email;
            return "$fn $ln";
        }else{
            return "Not auth yet.";
        }
    }
}
