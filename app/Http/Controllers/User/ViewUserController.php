<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

/**
 * EXTENDS USE
 */
use DB;
use Hash;

class ViewUserController extends Controller{

	public function getCredentials(){

		/**
		 * GET MASTER DATA.
		 */
		$result['result'] = [
			'prefixName' => $this->getPrefixName(),
			'position' => $this->getPosition(),
			'department' => $this->getDepartment(),
			'userRole' => $this->getUserRole(),
			'users' => $this->getUsersList()
		];

		return view('dashgum.user', $result);
	}

	public function postEditCredentials(Request $request, $id){
		$now = date("Y-m-d H:i:s");
		if($request->input('password') == ""){
			/**
			 * EDIT BY USING OLD PASSWORD.
			 */
			$user = DB::table('users')->where('usr_id', $id)->update([
				'usr_prefix_name' => $request->input('usr_prefix_name'),
				'usr_position' => $request->input('usr_position'),
				'usr_user_role' => $request->input('usr_user_role'),
				'usr_department' => $request->input('usr_department'),
				'usr_code' => $request->input('usr_code'),
				'usr_name' => $request->input('usr_name'),
				'usr_lastname' => $request->input('usr_lastname'),
				'email' => $request->input('email'),
				'usr_age' => $request->input('usr_age'),
				'usr_gender' => $request->input('usr_gender'),
				'usr_tel' => $request->input('usr_tel'),
				'usr_status' => $request->input('usr_status'),
				'updated_at' => $now
			]);

			$addr = DB::table('users_addr')->where('uaddr_user_id', $id)->update([
				'uaddr_user_id' => $user,
				'uaddr_no' => $request->input('uaddr_no'),
				'uaddr_village_name' => $request->input('uaddr_village_name'),
				'uaddr_street' => $request->input('uaddr_street'),
				'uaddr_town' => $request->input('uaddr_town'),
				'uaddr_city' => $request->input('uaddr_city'),
				'uaddr_province' => $request->input('uaddr_province'),
				'uaddr_postal' => $request->input('uaddr_postal'),
				'uaddr_tel' => $request->input('uaddr_tel'),
				'updated_at' => $now
			]);
		}elseif($request->input('password') != "" && ($request->input('password') == $request->input('conf_password'))){
			/**
			 * EDIT AND UPDATE NEW PASSWORD.
			 */
			$user = DB::table('users')->where('usr_id', $id)->update([
				'usr_prefix_name' => $request->input('usr_prefix_name'),
				'usr_position' => $request->input('usr_position'),
				'usr_user_role' => $request->input('usr_user_role'),
				'usr_department' => $request->input('usr_department'),
				'usr_code' => $request->input('usr_code'),
				'usr_name' => $request->input('usr_name'),
				'usr_lastname' => $request->input('usr_lastname'),
				'email' => $request->input('email'),
				'password' => Hash::make($request->input('password')),
				'usr_age' => $request->input('usr_age'),
				'usr_gender' => $request->input('usr_gender'),
				'usr_tel' => $request->input('usr_tel'),
				'usr_status' => $request->input('usr_status'),
				'updated_at' => $now
			]);

			$addr = DB::table('users_addr')->where('uaddr_user_id', $id)->update([
				'uaddr_user_id' => $user,
				'uaddr_no' => $request->input('uaddr_no'),
				'uaddr_village_name' => $request->input('uaddr_village_name'),
				'uaddr_street' => $request->input('uaddr_street'),
				'uaddr_town' => $request->input('uaddr_town'),
				'uaddr_city' => $request->input('uaddr_city'),
				'uaddr_province' => $request->input('uaddr_province'),
				'uaddr_postal' => $request->input('uaddr_postal'),
				'uaddr_tel' => $request->input('uaddr_tel'),
				'updated_at' => $now
			]);
		}else{
			return back()->with('errMSG', 'กรุณากรอก password ให้ตรงกัน');
		}
		return redirect('setting/user')->with('success', 'การแก้ไขข้อมูลสำเร็จ');
	}

	public function postAddCredentials(Request $request){
		if($request->input('password') == $request->input('conf_password')){
			$now = date("Y-m-d H:i:s");
			$user = DB::table('users')->insertGetId([
				'usr_prefix_name' => $request->input('usr_prefix_name'),
				'usr_position' => $request->input('usr_position'),
				'usr_user_role' => $request->input('usr_user_role'),
				'usr_department' => $request->input('usr_department'),
				'usr_code' => $request->input('usr_code'),
				'usr_name' => $request->input('usr_name'),
				'usr_lastname' => $request->input('usr_lastname'),
				'email' => $request->input('email'),
				'password' => Hash::make($request->input('password')),
				'usr_age' => $request->input('usr_age'),
				'usr_gender' => $request->input('usr_gender'),
				'usr_tel' => $request->input('usr_tel'),
				'usr_status' => $request->input('usr_status'),
				'created_at' => $now,
				'updated_at' => $now
			]);

			$addr = DB::table('users_addr')->insert([
				'uaddr_user_id' => $user,
				'uaddr_no' => $request->input('uaddr_no'),
				'uaddr_village_name' => $request->input('uaddr_village_name'),
				'uaddr_street' => $request->input('uaddr_street'),
				'uaddr_town' => $request->input('uaddr_town'),
				'uaddr_city' => $request->input('uaddr_city'),
				'uaddr_province' => $request->input('uaddr_province'),
				'uaddr_postal' => $request->input('uaddr_postal'),
				'uaddr_tel' => $request->input('uaddr_tel'),
				'created_at' => $now,	
				'updated_at' => $now
			]);

			return redirect('setting/user')->with('success', 'เพิ่มข้อมูลสำเร็จ');
		}else{
			return back()->with('errMSG', 'กรุณากรอก password ให้ตรงกัน');
		}
	}

	public function getCredentialsById($id){
		/**
		 * GET MASTER DATA.
		 */
		$result['result'] = [
			'prefixName' => $this->getPrefixName(),
			'position' => $this->getPosition(),
			'department' => $this->getDepartment(),
			'userRole' => $this->getUserRole(),
			'user' => $this->getUserById($id)
		];
		// print_r($result['result']['users']);

		return view('dashgum.user', $result);
	}

	/**
	 * PROTECTED.
	 */

	protected function getUserById($id){
		return DB::table('users')
				->leftJoin('users_addr', 'users.usr_id', '=', 'users_addr.uaddr_user_id')
				->select('users.*', 'users_addr.*')
				->where('usr_id', $id)
				->get();
	}

	protected function getUsersList(){
		return DB::table('users')->get();
	}

	protected function getPrefixName(){
		return DB::table('_prefix_name')
			->select('pfn_id', 'pfn_short_name', 'pfn_long_name')
			->get();
	}

	protected function getPosition(){
		return DB::table('_position')
			->select('pos_id', 'pos_short_name', 'pos_long_name')
			->get();
	}

	protected function getDepartment(){
		return DB::table('_department')
			->select('dep_id', 'dep_short_name', 'dep_long_name', 'dep_email', 'dep_tel')
			->get();
	}

	protected function getUserRole(){
		return DB::table('_user_role')
			->select('ur_id', 'ur_short_name', 'ur_long_name')
			->get();
	}
	
}
