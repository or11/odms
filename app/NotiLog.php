<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NotiLog extends Model
{
    /**
     * The Table
     * @var String
     */
    protected $table = 'notification_log';

    /**
     * The Primary Key
     * @var integer
     */
    protected $primaryKey = 'noti_id';


    /**
     * Time Stamps
     * @var Bool
     */
    public $timestamps = true;
}
