<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MSDS extends Model
{
    /**
     * The Table
     * @var String
     */
    protected $table = 'msds';

    /**
     * The Primary Key
     * @var integer
     */
    protected $primaryKey = 'msds_id';


    /**
     * Time Stamps
     * @var Bool
     */
    public $timestamps = true;
}
