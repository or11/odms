<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Standard extends Model
{
    /**
     * The Table
     * @var String
     */
    protected $table = 'standard';

    /**
     * The Primary Key
     * @var integer
     */
    protected $primaryKey = 'standard_id';


    /**
     * Time Stamps
     * @var Bool
     */
    public $timestamps = true;
}
