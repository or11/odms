<?php
namespace App\WesarutLibrary;


class Wesarut {

	
	public function dateDiff($startdate,$enddate){//หาค่า ระยะห่างของ start_date และ  end_end
		return (strtotime($enddate) - strtotime($startdate))/  ( 60 * 60 * 24 );  // 1 day = 60*60*24
	}

	/**
	*	convertDate
	*
	*
	*	This function is the short converting date format.
	*
	*	@param string $format The result date format.
	*	@param string $date original date.
	*	@return string return date in new format.
	*	@author Wesarut Khumwilai <wesarut.khm@gmail.com>
	*
	**/
	public function convertDate($format,$date){
		return date($format, strtotime($date));
	}
}