<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Mi extends Model
{
    /**
     * The Table
     * @var String
     */
    protected $table = 'mi';

    /**
     * The Primary Key
     * @var integer
     */
    protected $primaryKey = 'mi_id';


    /**
     * Time Stamps
     * @var Bool
     */
    public $timestamps = true;
}
