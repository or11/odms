<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SubDocument extends Model
{
    /**
     * The Table
     * @var String
     */
    protected $table = 'sub_document';

    /**
     * The Primary Key
     * @var integer
     */
    protected $primaryKey = 'subd_id';


    /**
     * Time Stamps
     * @var Bool
     */
    public $timestamps = true;
}
