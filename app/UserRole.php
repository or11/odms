<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserRole extends Model
{
    /**
     * The Table
     * @var String
     */
    protected $table = '_user_role';

    /**
     * The Primary Key
     * @var integer
     */
    protected $primaryKey = 'ur_id';


    /**
     * Time Stamps
     * @var Bool
     */
    public $timestamps = true;
}
