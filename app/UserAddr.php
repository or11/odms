<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserAddr extends Model
{
    /**
     * The Table
     * @var String
     */
    protected $table = 'users_addr';

    /**
     * The Primary Key
     * @var integer
     */
    protected $primaryKey = 'uaddr_id';


    /**
     * Time Stamps
     * @var Bool
     */
    public $timestamps = true;
}
