<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Department extends Model
{
    /**
     * The Table
     * @var String
     */
    protected $table = '_department';

    /**
     * The Primary Key
     * @var integer
     */
    protected $primaryKey = 'dep_id';


    /**
     * Time Stamps
     * @var Bool
     */
    public $timestamps = true;
}
