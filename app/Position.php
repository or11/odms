<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Position extends Model
{
    /**
     * The Table
     * @var String
     */
    protected $table = '_position';

    /**
     * The Primary Key
     * @var integer
     */
    protected $primaryKey = 'pos_id';


    /**
     * Time Stamps
     * @var Bool
     */
    public $timestamps = true;
}
