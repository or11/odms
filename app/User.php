<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    protected $table = 'users';

    protected $primaryKey = 'usr_id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'usr_code', 'usr_name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public $timestamps = true;



/**
 * CHECKING FOR AUTHORIZE.
 */
    public function isAdmin(){
        // this looks for an admin column in your users table
        return $this->usr_user_role == '1' ? true : false; 
    }
    public function isManager(){
        // this looks for an admin column in your users table
        return $this->usr_user_role == '2' ? true : false; 
    }
    public function isUser(){
        // this looks for an admin column in your users table
        return $this->usr_user_role == '3' ? true : false; 
    }
}
