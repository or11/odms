<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Chemicals extends Model
{
    /**
     * The Table
     * @var String
     */
    protected $table = '_chemicals';

    /**
     * The Primary Key
     * @var integer
     */
    protected $primaryKey = 'che_id';


    /**
     * Time Stamps
     * @var Bool
     */
    public $timestamps = true;
}
